import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { varStorageGlobal, IGetToken } from '../interfaces/public';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/fromPromise'
import 'rxjs/add/operator/mergeMap';
import { DbStorageService } from './db-storage.service';

const TOKEN_KEY = 'TOKEN-AUTHORIZATE';
@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor  {

  constructor(private storage: DbStorageService) {

   }
 
 
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // console.log("si");
    //Get Token Authenticate in local Storage
    let promise = this.storage.getToken();
        

    return Observable.fromPromise(promise)
      .mergeMap(token => {

        let getToken :IGetToken=token;      
        
        let Token="";
                  
        if(token){
          Token=getToken.token;
        }else{

        }
        //console.log(token);
        let clonedReq = this.addToken(request, Token);

       // console.log( clonedReq);
        return next.handle(clonedReq).pipe(
          catchError(error => {
            // Perhaps display an error for specific status codes here already?
            let msg = error.message;
           // console.log("*Error*=> " +msg);
            // Pass the error to the caller of the function
            return throwError(error);
          })
        );
      });
  }
  private addToken(request: HttpRequest<any>, token: any) {
  
  
    if (token) {
      let clone: HttpRequest<any>;
      clone = request.clone({
        setHeaders: {
          Accept: `application/json`,
          'Content-Type': `application/json`,
          Authorization: `Bearer ${token}`          
        }    
      });
      return clone;
    }
    return request;
  }


}

import { Injectable, ViewChild } from "@angular/core";
import * as signalR from "@aspnet/signalr";
import { DbStorageService } from "./db-storage.service";
import { resolve, reject } from "q";
import { IUserOnline } from "../interfaces/public";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SignalRService {
  callToggle = new Subject();
  // Observable string sources
  private componentMethodCallSource = new Subject<any>();
  // Observable string streams
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();


  fingeHub = "/finger";
  idConecction: string = "";
  constructor(private storage: DbStorageService) {
    }

  private hubConnection: signalR.HubConnection;

  public startConnection(): Promise<any> {
    const result = new Promise<any>((resolve, reject) => {
      this.storage.GetUrlApi().subscribe(data => {
        if (data) {
          //  var host="wss://srvdemo.ddns.net:44"+ this.fingeHub;

          let url: string = data + this.fingeHub;
          this.hubConnection = new signalR.HubConnectionBuilder()
            .withUrl(url)
            .build();

          this.hubConnection
            .start()
            .then(() => {
              resolve(true);
              console.log("Connection started");

              //if is http
              let getIdSession: string = this.hubConnection["connection"]
              .transport.webSocket.url;

              //if https 
              // let getIdSession: string = this.hubConnection["connection"]
              //   .transport.url

                //console.log(getIdSession)
              // .transport.webSocket.url;
              this.idConecction = getIdSession.split("=")[1];
              //console.log(this.idConecction)
              this.storage.getProfileUser().subscribe(data => {
                // console.log(data);
                if (data) {
                  let profileUser: IUserOnline = data;

                  setTimeout(() => {
                    this.callAddUser(
                      profileUser.id,
                      5555,
                      profileUser.idSecret
                    );
                  }, 500);
                }
              });
            })
            .catch(err => {
              console.log("Error while starting connection: " + err);
              reject(false);
            });
        }
      });
    });

    return result;
  }

  public endConnection(): Promise<any> {
    const result = new Promise<any>((resolve, reject) => {
      this.storage.GetUrlApi().subscribe(data => {
        if (data) {
          //let url: string = data + this.fingeHub;
          this.hubConnection
            .stop()
            .then(() => {
              resolve(true);
              //console.log("Connection started");
              // var connectionUrl = this.hubConnection["connection"].transport.webSocket.url ;
              //console.log(connectionUrl);
            })
            .catch(err => {
              console.log("Error while starting connection: " + err);
              reject(false);
            });
        }
      });
    });

    return result;
  }

  public statusConecction(): Promise<any> {
    const result = new Promise<any>((resolve, reject) => {
      var status: number = this.hubConnection.state;
      if (status == 1) {
        //console.log("Conectado")
        resolve(true);
      } else {
        resolve(false);
      }
    }).catch(error => {
      reject(false);
    });

    return result;
  }

  //#region "Managed found FingerPrint Punch employee online"

  public addSuscribeToNotifiPushOnline = () => {
    this.hubConnection.on("notifiPushEmployeeOnline", data => {
      //this.data = data;
      console.log("Receive : " + data);     
      // alert(data);
      this.componentMethodCallSource.next(data);
      // console.log(data);
    });
  };

  public callAddUser(idUser: number, idGroup: number, secret: string) {
    //IDUSer , IdConnection IdGroup  Secret  PlatformAPp
    this.hubConnection
      .invoke(
        "AddUserConnect",
        idUser,
        this.idConecction,
        idGroup,
        secret,
        "Movil App"
      )
      .then(data => {
        // console.log(data);
      })
      .catch(error => {
        // console.log(error);
      });
  }

  //#endregion

  // sendIdSessionUser(idUser:string,idSession) { this.hubConnection
  //   .invoke("SaveIdSessionUser", idUser,idSession)
  //   .then(data => {
  //     // console.log(data);
  //   })
  //   .catch(error => {
  //     // console.log(error);
  //   });}

  doSomething(value) {
    console.debug(value);
  }

  public Send(msg: string) {
    this.hubConnection
      .invoke("Send", "UsuarioX", msg)
      .then(data => {
        // console.log(data);
      })
      .catch(error => {
        // console.log(error);
      });
  }
}

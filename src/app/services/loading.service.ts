import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  idLoading="34";
  msg="Cargando Espere..."
  isLoading = false;
  constructor(public loadingController: LoadingController,
    public toastController: ToastController) { }

  // async show() {
  //   setTimeout(async () => {
  //     const loading = await this.loadingController.create({
  //       id: this.idLoading,
  //       message: this.msg
  //     });
  //     return await loading.present();
  //   }, 500);
   
   
  // }

  // async hide() {
  // setTimeout(async () => {
  //   return await this.loadingController.dismiss(null, null, this.idLoading);
  // },1000);
   
  // }

  toastShow(msg:string){
    this.presentToastWithOptions(msg);
  }
  

  async presentToastWithOptions(msg:string) {
    const toast = await this.toastController.create({
      color: 'danger',
        duration: 5000,
        message: msg,
        showCloseButton: true,
        closeButtonText:"Cerrar"
      // buttons: [
      //   {
      //     side: 'start',
      //     icon: 'star',
      //     text: 'Favorite',
      //     handler: () => {
      //       console.log('Favorite clicked');
      //     }
      //   }, {
      //     text: 'Done',
      //     role: 'cancel',
      //     handler: () => {
      //       console.log('Cancel clicked');
      //     }
      //   }
      // ]
    });
    toast.present();
  }


 
  async show() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 10000,
      message: this.msg
    }).then(a => {
      a.present().then(() => {
        ///console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => {});
        }
      });
    });
  }


  async showInfinity() {
    this.isLoading = true;
    return await this.loadingController.create({     
      message: this.msg
    }).then(a => {
      a.present().then(() => {
        ///console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => {});
        }
      });
    });
  }


  async hide() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => {});
  }
}

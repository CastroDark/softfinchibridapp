import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

const tz = "America/Santo_Domingo";
//import * as moment from 'moment';
import * as moment from "moment-timezone";
import { delay } from "rxjs/operators";
import { async } from "@angular/core/testing";
import { RouteReuseStrategy } from "@angular/router";
@Injectable({
  providedIn: "root"
})
export class DatetimeService {
  constructor() {
    //moment().tz("America/Santo_Domingo").format();
  }

  getDatetime(): Observable<any> {
    // moment()..setDefault("America/Santo_Domingo");
    const result = new Observable(observable => {
      // var jun = moment("2014-12-01T12:00:00Z");

      // jun.tz('America/Los_Angeles').format();  // 5am PDT
      //moment().tz('America/Caracas').format();
      observable.next(moment().format());

      //'
    });

    return result;
  }

  getFormatDate(date: Date): Observable<any> {
    // moment()..setDefault("America/New_York");
    const result = new Observable(observable => {
      var dayWeek = this.transFormDayString(moment(date).format("d"));
      var day = moment(date).format("DD");
      var month = moment(date).format("MM");
      var monthString = this.transFormMonthString(moment(date).format("MM"));
      var year = moment(date).format("YYYY");

      observable.next(dayWeek + " ," + day + " " + monthString + " " + year);
    });

    return result;
  }

  getFormatDateTime(datetime: string): Observable<any> {
    // moment().tsetDefault("America/New_York");
    const result = new Observable(observable => {
      // var dayWeek = this.transFormDayString(moment(date).tz(this.timeZone).format('d'));
      // var day = moment(date).tz(this.timeZone).format('DD');
      // var month = moment(date).tz(this.timeZone).format('MM');
      // var monthString = this.transFormMonthString(moment(date).tz(this.timeZone).format('MM'));
      // var year = moment(date).tz(this.timeZone).format('YYYY');

      var send = moment(new Date(datetime)).format("hh:mm:ss A");

      observable.next(send);
    });

    return result;
  }

  transFormDayString(day: string) {
    switch (day) {
      case "0":
        return "Domingo";
        break;
      case "1":
        return "Lunes";

        break;
      case "2":
        return "Mart";

      case "3":
        return "Mier";

      case "4":
        return "Juev";

      case "5":
        return "Vier";

      case "6":
        return "Sab";

      default:
        return "null";
        break;
    }
  }

  transFormMonthString(month: string) {
    switch (month) {
      case "01":
        return "Ene";

      case "02":
        return "Feb";

      case "03":
        return "Mar";
      case "04":
        return "Abr";

      case "05":
        return "May";

      case "05":
        return "Jun";

      case "07":
        return "Jul";
      case "08":
        return "Ago";
      case "09":
        return "Sep";
      case "10":
        return "Oct";
      case "11":
        return "Nov";
      case "12":
        return "Dic";

      default:
        return "null";
    }
  }

  getUniqueId(): Observable<number> {
    // moment.tz.setDefault("America/Santo_Domingo");
    const result = new Observable<number>(observe => {
      setTimeout(async () => {    
        var dateSend = await this.returnFecha();//moment( await this.returnFecha()).format("YYDDMMHmmss");
        observe.next(dateSend);
      },500);
    });
    return result;
  }

 async returnFecha() {
 // console.log(new Date(Date.now()))
  await delay(0);     
  return await Date.now()
 
  }
}

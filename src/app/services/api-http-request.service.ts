import { Injectable } from "@angular/core";
import "rxjs/add/operator/timeout";

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { HttpRequest } from "@angular/common/http";

import {
  IResultModelBasic,
  IProfilePhone,
  IUser,
  IGetToken,
  IUserOnline,
  IProfileCompany,
  ICheckStatusTOken,
  ISendDataRequestBasic
} from "../interfaces/public";
import { DbStorageService } from "./db-storage.service";
import { varStorageGlobal, IEmployee } from '../interfaces/public';
import { Platform } from "@ionic/angular";

const setTime: number = 4000;

@Injectable({
  providedIn: "root"
})
export class ApiHttpRequestService {
  urlHttp: string = "";

  // basePath="/api"

  constructor(
    public http: HttpClient,
    private storage: DbStorageService,
    private plt: Platform
  ) {
    // if (this.plt.is("cordova")) {

    //   this.storage.GetUrlApi().subscribe(data => {
    //     this.urlHttp = data;
    //   });
    // } else {
    // }

    this.storage.GetUrlApi().subscribe(data => {
      this.urlHttp = data;
    });
  }

  checkStatusServer(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.storage.GetUrlApi().subscribe(
        data => {
          this.CheckUrlApi(data).subscribe(
            data => {
              observe.next(true);
            },
            error => {
              observe.error(false);
            }
          );
        },
        error => {
          observe.error(false);
        }
      );
    });
    return result;
  }

  //#region Manage Status and Authorizate phone

  public CheckUrlApi(url: string): Observable<IResultModelBasic> {
    return this.http
      .post<IResultModelBasic>(`${url}/api/authorize/checkapi`, {})
      .timeout(setTime);
  }

  //check Autorize app
  public CheckAutorizeApp(modelSend: any): Observable<IResultModelBasic> {
    return this.http
      .post<IResultModelBasic>(
        `${this.urlHttp}/api/authorize/checkautorizeapp`,
        modelSend
      )
      .timeout(setTime);
  }

  //get profile phone APp
  public GetProfilePhone(modelSend: any): Observable<IProfilePhone> {
    return this.http
      .post<IProfilePhone>(
        `${this.urlHttp}/api/authorize/sendprofilephone`,
        modelSend
      )
      .timeout(setTime);
  }


  public SendUpdateDataApp(idDevice: string): Observable<any> {
    var fecha = "2019-01-01";
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1:idDevice,
      query2: ""
    };

    return this.http.post<any>(
      `${this.urlHttp}/api/webmethods/getauthorizetochangeupdatedataapp`,
      data
    );
  }

  //#endregion

  //#region Manage Authorizate user and login

  public validateUser(data: IUser): Observable<IGetToken> {
    return this.http
      .post<IGetToken>(`${this.urlHttp}/api/authorize/loginuser`, data)
      .timeout(setTime);
  }

  //Get PRofile User Online
  public GetProfileUserOnline(): Observable<IUserOnline> {
    return this.http
      .post<IUserOnline>(
        `${this.urlHttp}/api/authorize/SendProfileUserOnline`,
        {}
      )
      .timeout(setTime);
  }

  public GetProfileCompany(): Observable<IProfileCompany> {
    return this.http
      .post<IProfileCompany>(
        `${this.urlHttp}/api/authorize/sendprofilecompany`,
        {}
      )
      .timeout(setTime);
  }

  //checkStatusToken
  public CheckStatusToken(
    idUser: number,
    oldToken
  ): Observable<ICheckStatusTOken> {
    var model = {
      idUser: idUser,
      oldToken: oldToken
    };
    return this.http
      .post<ICheckStatusTOken>(
        `${this.urlHttp}/api/authorize/checkstatustokenjwt`,
        model
      )
      .timeout(setTime);
  }
  //#endregion

  //#region Managed FingerPrint
  public GetReportAssistanceDate(fecha: string): Observable<any> {
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: "",
      query2: ""
    };

    return this.http
      .post<any>(
        `${this.urlHttp}/api/webmethods/sendreportassistancefordate`,
        data
      )
      .timeout(setTime);
  }

  //#endregion

  //#region Manage Get data Config GLobal

  public GetIfUpdateDataApp(idApp: number): Observable<any> {
    var fecha = "2019-01-01";
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: String(idApp),
      query2: ""
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/SendIfAppUpdateData`, data)
      .timeout(setTime);
  }

  public SendInfoUpdateDataFinishApp(
    idApp: number,
    field: string,
    value: string
  ): Observable<any> {
    var fecha = "2019-01-01";
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: String(idApp),
      query2: field + "*" + value
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/GetUpdateDataFinishApp`, data)
      .timeout(setTime);
  }

  public GetListGroupsEmployees(): Observable<any> {
    var fecha = "2019-01-01";
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: "",
      query2: ""
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/sendlistgroupsemployees`, data)
      .timeout(setTime);
  }

  public GetListOccupationEmployees(): Observable<any> {
    var fecha = "2019-01-01";
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: "",
      query2: ""
    };

    return this.http
      .post<any>(
        `${this.urlHttp}/api/webmethods/sendlistoccupationemployees`,
        data
      )
      .timeout(setTime);
  }

  public SendBase64ToSaveFileServer(
    base64: string,
    idEmployee: string,
    fileName
  ): Observable<any> {
    let data = {
      imgBase64: base64,
      idEmployee: idEmployee,
      fileName: fileName
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/getbase64tosaveimg`, data)
      .timeout(setTime);
  }

  public SendFilenameToGetBase64(
    idEmployee: string,
    fileName: string
  ): Observable<any> {
    let data = {
      imgBase64: "",
      idEmployee: idEmployee,
      fileName: fileName
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/GetFileNametoSendBase64`, data)
      .timeout(setTime);
  }

  public GetImgBase64AllEmployeesForIdGroup(idGroup: string): Observable<any> {
    var fecha = "2019-01-01";
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: idGroup,
      query2: ""
    };

    return this.http.post<any>(
      `${this.urlHttp}/api/webmethods/SendListImgagesEmployeeForIdGroup`,
      data
    );
  }

  public GetListCurrencys(): Observable<any> {
    return this.http.post<any>(
      `${this.urlHttp}/api/webmethods/sendlistcurrencys`,
      {}
    );
  }

  public GetListPaysRoll(): Observable<any> {
    return this.http.post<any>(
      `${this.urlHttp}/api/webmethods/sendlistpaysroll`,
      {}
    );
  }

  //#endregion

  //#region Managed Employees
  public GetListEmployeeServer(fecha: string): Observable<any> {
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: "",
      query2: ""
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/sendlistemployees`, data)
      .timeout(setTime);
  }

  public SendAddOfUpdateEmployee(employee:IEmployee): Observable<any> {
    

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/getaddofupdateemployee`, employee)
      .timeout(setTime);
  }

  //#endregion

  //#region Managed Devices Punch
  public GetListDevicesPunch(fecha: string): Observable<any> {
    let data: ISendDataRequestBasic = {
      date1: fecha,
      date2: fecha,
      query1: "",
      query2: ""
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/sendlistdevicespunch`, data)
      .timeout(setTime);
  }

  public SendModeDeviceToUpdate(idDevice: number, modeDevice): Observable<any> {
    let data: ISendDataRequestBasic = {
      date1: "2019-01-01",
      date2: "2019-01-01",
      query1: String(idDevice),
      query2: modeDevice
    };

    return this.http
      .post<any>(`${this.urlHttp}/api/webmethods/getmodedevicetoupdate`, data)
      .timeout(setTime);
  }
  //#endregion
}

interface demo {
  Accept: string;
  "Content-Type": string;
  Authorization: string;
}

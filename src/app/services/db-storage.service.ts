import { Injectable } from "@angular/core";

import { Storage } from "@ionic/storage";
import { Observable, observable } from "rxjs";
import {
  IGetToken,
  IShowProfileEmployee,
  ICurrency,
  IProfileGeneralEmployeeShow
} from "../interfaces/public";
import { FilesService } from "./files.service";
import {
  IProfileCompany,
  IEmployee,
  IGroupEmployee,
  IPayRoll,
  IOccupationEmployee,
  IListImages
} from "../interfaces/public";
import {
  varStorageGlobal,
  IProfilePhone,
  IUserOnline
} from "../interfaces/public";
import { DomSanitizer } from "@angular/platform-browser";
import { platform } from "os";
import { Platform } from "@ionic/angular";
import { resolve } from "path";
import { reject } from "q";
import { analyzeAndValidateNgModules } from "@angular/compiler";

@Injectable({
  providedIn: "root"
})
export class DbStorageService {
  constructor(
    private storage: Storage,
    private files: FilesService,
    public _DomSanitizer: DomSanitizer,
    private platCtrl: Platform
  ) {}

  dbEmployees: Array<IEmployee> = [];
  dbListGroups: Array<IGroupEmployee> = [];
  dbListOccupationsEmployees: Array<IOccupationEmployee> = [];
  dbListImg64: Array<IListImages> = [];
  dbListCurrency: Array<ICurrency> = [];
  dbListPayRoll: Array<IPayRoll> = [];
  dbListsNationality = [
    {
      id: "1",
      name: "Dominicana"
    },
    {
      id: "2",
      name: "Haitiana"
    }
  ];
  dbListsModeCheckAssistance = [
    {
      id: "1",
      name: "Movil"
    },
    {
      id: "2",
      name: "Ponche"
    },
    {
      id: "3",
      name: "Automatico"
    }
  ];
  dbListsSegurityPolicy = [
    {
      id: "1",
      name: "Grupo #1"
    },
    {
      id: "2",
      name: "Grupo #2"
    },
    {
      id: "3",
      name: "Grupo #3"
    }
  ];

  //#region Manage Authorizate phone

  //get urlApi
  GetUrlApi(): Observable<any> {
    // alert("ssssis") ;
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.get(varStorageGlobal.URL_API).then(
          data => {
            //alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }

  //save url APi to storage
  saveUrlApi(url: string): Observable<any> {
    // alert("ssssis") ;
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.set(varStorageGlobal.URL_API, url).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }

  saveSerialAuthorize(registerId: string): Observable<any> {
    // alert("ssssis") ;
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.set(varStorageGlobal.IDREGISTERPHONE, registerId).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }
  getSerialAuthorize(): Observable<any> {
    // alert("ssssis") ;
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.get(varStorageGlobal.IDREGISTERPHONE).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }

  saveProfilePhone(profilePhone: IProfilePhone): Observable<any> {
    // alert("ssssis") ;
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.set(varStorageGlobal.PROFILEPHONE, profilePhone).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }
  getProfilePhone(): Observable<any> {
    // alert("ssssis") ;
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.get(varStorageGlobal.PROFILEPHONE).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }

  //#endregion

  //#region Manage Login User

  saveToken(token: IGetToken): Observable<any> {
    // alert("ssssis") ;
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.set(varStorageGlobal.TOKEN, token).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }
  getToken(): Promise<any> {
    let result = new Promise((resolve, reject) => {
      this.storage.get(varStorageGlobal.TOKEN).then(
        data => {
          resolve(data);
        },
        error => {
          reject("");
        }
      );
    });
    return result;
  }
  removeToken(): Observable<any> {
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.remove(varStorageGlobal.TOKEN).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }

  saveProfileUser(profileUser: IUserOnline): Observable<any> {
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.set(varStorageGlobal.USER_ONLINE, profileUser).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }
  getProfileUser(): Observable<IUserOnline> {
    const studentsObservable = new Observable<IUserOnline>(observer => {
      setTimeout(() => {
        this.storage.get(varStorageGlobal.USER_ONLINE).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }
  removeProfileUser(): Observable<any> {
    const studentsObservable = new Observable<any>(observer => {
      setTimeout(() => {
        this.storage.remove(varStorageGlobal.USER_ONLINE).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }

  saveProfileCompany(profileCompany: IProfileCompany): Observable<any> {
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.set(varStorageGlobal.PROFILE_COMPANY, profileCompany).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }
  getProfileCompany(): Observable<IProfileCompany> {
    const studentsObservable = new Observable<IProfileCompany>(observer => {
      setTimeout(() => {
        this.storage.get(varStorageGlobal.PROFILE_COMPANY).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }
  removeProfileCompany(): Observable<any> {
    const studentsObservable = new Observable<any>(observer => {
      setTimeout(() => {
        this.storage.remove(varStorageGlobal.PROFILE_COMPANY).then(
          data => {
            // alert(data);
            observer.next(data);
          },
          error => {
            observer.error(error);
            //alert(error);
          }
        );
      }, 0);
    });

    return studentsObservable;
  }

  //#endregion

  //#region MANAGE DATA EMPLOYEES

  public saveDataEMployees(list: Array<IEmployee>) {
    this.storage.remove(varStorageGlobal.DB_EMPLOYEES).then(data => {
      this.storage.set(varStorageGlobal.DB_EMPLOYEES, list).then(data => {
        //console.log(list);
      });
    });
  }

  public getEmployeeToId(idEmployee: number): Observable<IEmployee> {
    const send = new Observable<IEmployee>(observe => {
      this.storage.get(varStorageGlobal.DB_EMPLOYEES).then(data => {
        var list: Array<IEmployee> = data;
        var result: IEmployee = list.filter(item => item.Id === idEmployee)[0];
        observe.next(result);
      });
    });
    return send;
  }
  public getListAllEmployees(): Observable<Array<IEmployee>> {
    const send = new Observable<Array<IEmployee>>(observe => {
      this.storage.get(varStorageGlobal.DB_EMPLOYEES).then(data => {
        var list: Array<IEmployee> = data;
        observe.next(list);
      });
    });
    return send;
  }

  public getListAllEmployeesToStatusIdgroup(
    status: number,
    group: number,
    textSearch: string
  ): Observable<Array<IEmployee>> {
    const send = new Observable<Array<IEmployee>>(observe => {
      this.storage.get(varStorageGlobal.DB_EMPLOYEES).then(data => {
        var list: Array<IEmployee> = data;

        switch (status) {
          case 0:
            break;
          case 1:
            list = list.filter(x => x.Status === 1);
            break;
          case 2:
            list = list.filter(x => x.Status === 0);
            break;
          default:
            break;
        }

        switch (group) {
          case 0:
            //console.log("not need GRoup");
            break;

          default:
            list = list.filter(x => x.IdGroup === group);
            // console.log("if need group");
            break;
        }
              
       textSearch= textSearch.toLowerCase();

        //filter nickname and name
        list = list.filter(
          it =>
            it.NickName.toLowerCase().includes(textSearch) ||
            it.Name.toLowerCase().includes(textSearch)
        );

        observe.next(list);
      });
    });
    return send;
  }

  public getEmployeeProfileToId(
    idEmployee: number
  ): Observable<IShowProfileEmployee> {
    const result = new Observable<IShowProfileEmployee>(observe => {
      let employeeSend: IShowProfileEmployee;
      this.getAllDataEmployee().subscribe(data => {
        let imgTransform = "";
        var employee: IEmployee = this.dbEmployees.filter(
          item => item.Id === idEmployee
        )[0];
        var group: IGroupEmployee = this.dbListGroups.filter(
          i => i.Id === employee.IdGroup
        )[0];
        var occupationsEmplo: IOccupationEmployee = this.dbListOccupationsEmployees.filter(
          x => x.Id === employee.IdOccupation
        )[0];

        if (this.platCtrl.is("cordova")) {
          if (this.dbListImg64) {
            var imgBase: IListImages = this.dbListImg64.filter(x =>
              x.name.includes(employee.PathImg)
            )[0];
            //console.log(imgBase);
            if (imgBase) {
              imgTransform = imgBase.base64;
            } else {
              imgTransform = this.files.img;
            }
          } else {
            imgTransform = this.files.img;
          }
        } else {
          imgTransform = this.files.img;
        }

        employeeSend = {
          contacts: employee.Contacts,
          nameGroup: group.Name,
          nickName: employee.NickName,
          image: this._DomSanitizer.bypassSecurityTrustUrl(imgTransform),
          name: employee.Name
        };
        observe.next(employeeSend);
      });
    });

    return result;
  }

  public updateEmployeeId(employee: IEmployee): Promise<any> {
    const result = new Promise<any>((resolve, reject) => {
      let listEmployees: Array<IEmployee> = [];
      this.getListAllEmployees().subscribe(data => {
        listEmployees = data;
        // console.log(listEmployees);
        const index: number = listEmployees.findIndex(
          x => x.Id === employee.Id
        );
        //console.log(index);
        if (index !== -1) {
          listEmployees.splice(index, 1);
        }
        listEmployees.push(employee);

        this.saveDataEMployees(listEmployees);

        resolve(true);
      });
    });
    return result;
  }

  public addNewEmployee(employee: IEmployee): Promise<any> {
    let listEmployee: Array<IEmployee>;
    const result = new Promise<any>((resolve, reject) => {
      this.getListAllEmployees().subscribe(data => {
        listEmployee = data;
        listEmployee.push(employee);
        this.storage.remove(varStorageGlobal.DB_EMPLOYEES).then(data => {
          this.storage
            .set(varStorageGlobal.DB_EMPLOYEES, listEmployee)
            .then(data => {
              resolve(data);
            });
        });
      });
    });

    return result;
  }

  public getAllProfileShowEmployees(listEmployee: Array<number>): Promise<Array<IProfileGeneralEmployeeShow>>
   {
    let listSendEmployee: Array<IProfileGeneralEmployeeShow> = [];

    const result = new Promise<any>((resolve, reject) => {
      this.getAllDataEmployee().subscribe(data => {
        let imgTransform: any;

        for (let item of listEmployee) {
          this.getEmployeeToId(Number(item)).subscribe(async data => {
            var employee: IEmployee = data;

            // if (this.platCtrl.is("cordova")) {
            // } else {
            //   imgTransform = this.files.img;
            // }
            if (this.dbListImg64) {
              var imgBase: IListImages = this.dbListImg64.filter(x =>
                x.name.includes(employee.PathImg)
              )[0];
              //console.log(imgBase);
              if (imgBase) {
                imgTransform = imgBase.base64;
              } else {
                imgTransform = this.files.img;
              }
            } else {
              imgTransform = this.files.img;
            }

            if (employee) {
              var profileEmployee: IProfileGeneralEmployeeShow = {
                address: employee.Address,
                birthDate: employee.BirthDate,
                contacts: employee.Contacts,
                currency: this.dbListCurrency.filter(
                  x => x.Id === employee.IdCurrency
                )[0].Name,
                dateCreate: employee.DateInsert,
                document: employee.DocumentEmployee,
                email: employee.Email,
                existFingerPrint: employee.ExistFingerPrint,
                groupName: this.dbListGroups.filter(
                  x => x.Id === employee.IdGroup
                )[0].Name,
                idGroup: employee.IdGroup,
                id: employee.Id,
                idCompany: employee.IdCompany,
                ignoreWorkContract: employee.IgnoreWorkContract,
                lastName: employee.LastName,
                modeCheckAssistance: "",
                name: employee.Name,
                nationality: this.dbListsNationality.filter(
                  x => x.id === String(employee.IdNationality)
                )[0].name,
                nickName: employee.NickName,
                note: employee.Note,
                occupation: this.dbListOccupationsEmployees.filter(
                  x => x.Id === employee.IdOccupation
                )[0].Name,
                roster: "",
                segurityPolicy: this.dbListsSegurityPolicy.filter(
                  x => x.id === String(employee.IdSegurityPolicy)
                )[0].name,
                status: employee.Status,
                wordDays: "",
                image: this._DomSanitizer.bypassSecurityTrustUrl(imgTransform)
              };
              listSendEmployee.push(profileEmployee);
            }
          });
        }

        setTimeout(() => {
          //console.log(data);
          resolve(listSendEmployee);
        }, 1000);
      });
    });

    return result;
  }

  getAllDataEmployee(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      //get employees to database
      this.getListAllEmployees().subscribe(async data => {
        this.dbEmployees = data;

        //get GroupsEmployees
        this.getListAllGroupsEmployees().subscribe(async data => {
          this.dbListGroups = data;
        });

        //get Occupations employee
        this.getListAllOccupationEmployees().subscribe(async data => {
          this.dbListOccupationsEmployees = data;
        });

        //get List Images Base64 Employee
        this.getListALlImgBase64().subscribe(data => {
          this.dbListImg64 = data;
          this.getAllListCurrency().subscribe(data => {
            this.dbListCurrency = data;
            this.getAllListPayRoll().subscribe(data => {
              this.dbListPayRoll = data;
              // for(let item of this.dbListImg64){
              //   if(item.name=="1194845877.jpg"){
              //     alert("si esta");
              //   }
              // console.log(data);
              //}
              observe.next(true);
            });
          });
        });
      });
    });

    return result;
  }

  //#endregion

  //#region MANAGE DATA IGroupsEmployees

  public saveDataGroupsEmployees(list: Array<IGroupEmployee>) {
    this.storage.remove(varStorageGlobal.DB_GROUPSEMPLOYEES).then(data => {
      this.storage
        .set(varStorageGlobal.DB_GROUPSEMPLOYEES, list)
        .then(data => {});
    });
  }
  public getGroupEmployeeToId(idGroup: number): Observable<IGroupEmployee> {
    const send = new Observable<IGroupEmployee>(observe => {
      this.storage.get(varStorageGlobal.DB_GROUPSEMPLOYEES).then(data => {
        var list: Array<IGroupEmployee> = data;
        var result: IGroupEmployee = list.filter(
          item => item.Id === idGroup
        )[0];
        observe.next(result);
      });
    });
    return send;
  }
  public getListAllGroupsEmployees(): Observable<Array<IGroupEmployee>> {
    const send = new Observable<Array<IGroupEmployee>>(observe => {
      this.storage.get(varStorageGlobal.DB_GROUPSEMPLOYEES).then(data => {
        var list: Array<IGroupEmployee> = data;
        observe.next(list);
      });
    });
    return send;
  }
  //#endregion

  //#region MANAGE DATA IOccupationsEmployees

  public saveDataOccupationEmployees(list: Array<IOccupationEmployee>) {
    this.storage.remove(varStorageGlobal.DB_OCCUPATIONEMPLOYEES).then(data => {
      this.storage
        .set(varStorageGlobal.DB_OCCUPATIONEMPLOYEES, list)
        .then(data => {
          // console.log(data)
        });
    });
  }
  public getOccupationEmployeeToId(
    idGroup: number
  ): Observable<IOccupationEmployee> {
    const send = new Observable<IOccupationEmployee>(observe => {
      this.storage.get(varStorageGlobal.DB_OCCUPATIONEMPLOYEES).then(data => {
        var list: Array<IOccupationEmployee> = data;
        var result: IOccupationEmployee = list.filter(
          item => item.Id === idGroup
        )[0];
        observe.next(result);
      });
    });
    return send;
  }
  public getListAllOccupationEmployees(): Observable<
    Array<IOccupationEmployee>
  > {
    const send = new Observable<Array<IOccupationEmployee>>(observe => {
      this.storage.get(varStorageGlobal.DB_OCCUPATIONEMPLOYEES).then(data => {
        var list: Array<IOccupationEmployee> = data;
        observe.next(list);
      });
    });
    return send;
  }
  //#endregion

  //#region MANAGE DATA CURRENCYS

  public saveListCurrency(list: Array<ICurrency>) {
    this.storage.remove(varStorageGlobal.DB_CURRENCYS).then(data => {
      this.storage.set(varStorageGlobal.DB_CURRENCYS, list).then(data => {
        // console.log(data)
      });
    });
  }
  public getCurrencyId(id: number): Observable<ICurrency> {
    const send = new Observable<ICurrency>(observe => {
      this.storage.get(varStorageGlobal.DB_CURRENCYS).then(data => {
        var list: Array<ICurrency> = data;
        var result: ICurrency = list.filter(item => item.Id === id)[0];
        observe.next(result);
      });
    });
    return send;
  }
  public getAllListCurrency(): Observable<Array<ICurrency>> {
    const send = new Observable<Array<ICurrency>>(observe => {
      this.storage.get(varStorageGlobal.DB_CURRENCYS).then(data => {
        var list: Array<ICurrency> = data;
        observe.next(list);
      });
    });
    return send;
  }
  //#endregion

  //#region MANAGE DATA PAYROLLS

  public saveListPayRoll(list: Array<IPayRoll>) {
    this.storage.remove(varStorageGlobal.DB_PAYROLL).then(data => {
      this.storage.set(varStorageGlobal.DB_PAYROLL, list).then(data => {
        // console.log(data)
      });
    });
  }
  public getPayRollId(id: number): Observable<IPayRoll> {
    const send = new Observable<IPayRoll>(observe => {
      this.storage.get(varStorageGlobal.DB_PAYROLL).then(data => {
        var list: Array<IPayRoll> = data;
        var result: IPayRoll = list.filter(item => item.Id === id)[0];
        observe.next(result);
      });
    });
    return send;
  }
  public getAllListPayRoll(): Observable<Array<IPayRoll>> {
    const send = new Observable<Array<IPayRoll>>(observe => {
      this.storage.get(varStorageGlobal.DB_PAYROLL).then(data => {
        var list: Array<IPayRoll> = data;
        observe.next(list);
      });
    });
    return send;
  }
  //#endregion

  //#region Manage List Images EMployeess

  public saveListImageBase64Employees(list: Array<IListImages>) {
    let newData: IListImages[] = list;
    let oldData: IListImages[] = [];

    this.storage.get(varStorageGlobal.DB_LISTIMAGES).then(data => {
      // console.log(data)
      oldData = data;
      if (oldData) {
        for (let item of newData) {
          //remove oldField
          const index = oldData.findIndex(x => x.name === item.name);
          if (index != -1) {
            //remove Found Index
            oldData.splice(index, 1);
          }
          //add new item
          oldData.push(item);
        }
      } else {
        oldData = newData;
      }
      this.storage
        .set(varStorageGlobal.DB_LISTIMAGES, oldData)
        .then(data => {});
    });
  }

  public deleteListImgBase64Employees() {
    this.storage.remove(varStorageGlobal.DB_LISTIMAGES).then(data => {});
  }

  public getImgBase64EmployeesToName(name: string): Observable<IListImages> {
    const send = new Observable<IListImages>(observe => {
      var list: Array<IListImages> = [];

      this.storage.get(varStorageGlobal.DB_LISTIMAGES).then(data => {
        if (data) {
          list = data;
          var result: IListImages = list.filter(item => item.name === name)[0];
          observe.next(result);
        } else {
          observe.next(result);
        }
      });
    });
    return send;
  }
  public getListALlImgBase64(): Observable<Array<IListImages>> {
    const send = new Observable<Array<IListImages>>(observe => {
      this.storage.get(varStorageGlobal.DB_LISTIMAGES).then(data => {
        var list: Array<IListImages> = data;
        observe.next(list);
      });
    });
    return send;
  }
  //#endregion

  //#region Manage CleanData
  public cleanDataDb(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.storage.remove(varStorageGlobal.DB_EMPLOYEES).then(() => {
        this.storage.remove(varStorageGlobal.DB_GROUPSEMPLOYEES).then(() => {
          this.storage.remove(varStorageGlobal.DB_LISTIMAGES).then(() => {
            this.storage
              .remove(varStorageGlobal.DB_OCCUPATIONEMPLOYEES)
              .then(() => {
                this.storage
                  .remove(varStorageGlobal.PROFILE_COMPANY)
                  .then(() => {
                    this.storage
                      .remove(varStorageGlobal.USER_ONLINE)
                      .then(() => {
                        this.storage.remove(varStorageGlobal.TOKEN).then(() => {
                          this.storage
                            .remove(varStorageGlobal.DB_PAYROLL)
                            .then(() => {
                              this.storage
                                .remove(varStorageGlobal.DB_CURRENCYS)
                                .then(() => {
                                  observe.next(true);
                                });
                            });
                        });
                      });
                  });
              });
          });
        });
      });
    });

    return result;
  }

  public clearDataChangeCompany(): Observable<boolean> {
    const result = new Observable<boolean>(Observe => {});

    return result;
  }

  //#endregion
}

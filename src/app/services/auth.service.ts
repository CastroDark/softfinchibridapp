import { Injectable } from "@angular/core";
import { DbStorageService } from "./db-storage.service";
import { ApiHttpRequestService } from "./api-http-request.service";
import { Observable } from "rxjs";
import { IUser, IUserOnline, varStorageGlobal, IGetToken, ICheckStatusTOken, IProfileCompany } from '../interfaces/public';
import { Router } from '@angular/router';

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(
    private storage: DbStorageService,
    private http: ApiHttpRequestService,
    private router:Router
  ) {}

  checkLoginUser(userData: IUser): Observable<number> {
    const result = new Observable<number>(observe => {
      this.http.validateUser(userData).subscribe(
        data => {
          if (data.token != "") {
            // console.log("token:" + data.token);
            //console.log("Si Logeado");
                  let tokenNew :IGetToken={
                    token:data.token,
                    expiration:"2019-01-01",
                    statusUser:1
                  }

            this.storage.saveToken(tokenNew).subscribe(data => {
              this.saveProfileUser().subscribe(data => {});
              this.saveProfileCompany().subscribe(data => {    
                observe.next(1);
              });
            });
          } else {
            //console.log("no logeado");
            this.logoutUser().subscribe(data => {
              observe.next(2);
            });
          }
        },
        error => {    
          //console.log("No Conecction")    
          //if fails connection check profile save in last login 
          this.checkUserDataLogin().subscribe(data=>{
            observe.next(data);;
          });        
        }
      );
    });

    return result;
  }

  saveProfileUser(): Observable<any> {
    const result = new Observable<any>(observe => {
      this.http.GetProfileUserOnline().subscribe(data => {
        this.storage.saveProfileUser(data).subscribe(data => {});
      });
    });

    return result;
  }

  saveProfileCompany(): Observable<any> {
    const result = new Observable<any>(observe => {
      this.http.GetProfileCompany().subscribe(data => {
        this.storage.saveProfileCompany(data).subscribe(data => {
          observe.next(data);
        });
      });
    });

    return result;
  }

  logoutUser(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.storage.removeToken().subscribe(data => {
        this.storage.removeProfileUser().subscribe(data => {
          this.storage.removeProfileCompany().subscribe(data => {});
          observe.next(true);
        });
      });
    });

    return result;
  }

  checkUserDataLogin(): Observable<number> {
    const result = new Observable<number>(observe => {
      this.storage.getProfileUser().subscribe(data => {
        if(data){
          observe.next(1);
        }else{
          observe.next(3);
        }        
      });
    });
    return result;
  }

  updateProfileCompany(): Observable<any> {
    const result = new Observable<any>(observe => {
      this.http.GetProfileCompany().subscribe(_dataProfile => {
        this.storage.removeProfileCompany().subscribe(data=>{
          this.storage.saveProfileCompany(_dataProfile).subscribe(data => {
            observe.next(data);
          });
        });        
      });
    });

    return result;
  }


  update


  //#region Check Token 


  execCheckToken(): Observable<any> {

    const result = new Observable(observer => {
      setTimeout(() => {
        this.storage.getToken().then(token => {           
         //if Exist Token
          if (token) {            
            
            //checkStatus TokenUser in server
            this.checkStatusToken().subscribe(data => {             
              let resul: ICheckStatusTOken = data;
              if (resul.status == 0) {
                this.InvalidToken();
                // console.log("invalid token")
              } if (resul.status == 1) {
                //token Status ok
                // console.log("token ok")
              } if (resul.status == 2) {
                //console.log("update token")
                this.updateToken(resul).subscribe(data => {
                });
              }
              observer.next(data);

            }, error => {
              observer.error(error);
            })

          }else{
            //not exist token in storage
            observer.error(null);
          }
        
        });
      }, 0);
    });

    return result;



  }

  checkStatusToken(): Observable<any> {

    const NObservable = new Observable(observer => {
      setTimeout(() => {
        this.storage.getToken().then(token => {
          let resultToken: IGetToken = token;
          this.storage.getProfileUser().subscribe(user => {
            let profileUser: IUserOnline = user;
            this.http.CheckStatusToken(profileUser.id, resultToken.token).subscribe(data => {
              //console.log(data);
              observer.next(data);
            }, error => {
              observer.error(error);
            });
          });
        })
      }, 0);
    });

    return NObservable;

  }
  
  InvalidToken() {
    //token expire or invalid
   //this.loading.show();
    setTimeout(() => {
      this.logoutUser().subscribe(result => {
        this.router.navigate(['/login'])
      });
     // this.loading.hide();
    }, 2000);

  };

  updateToken(token: ICheckStatusTOken): Observable<any> {


    const NObservable = new Observable(observer => {
      setTimeout(() => {

        //remove token 
        this.storage.removeToken().subscribe(remove => {
        
          let newToken: IGetToken = {
            expiration: token.expiration,
            token: token.newToken,
            statusUser: token.status
          }

          this.storage.saveToken(newToken).subscribe(data => {
            observer.next(data);
          });
        });
      }, 0);
    });

    return NObservable;


  }



  //#endregion
}

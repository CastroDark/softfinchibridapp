import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AuthorizePhonePage } from './authorize-phone.page';

const routes: Routes = [
  {
    path: '',
    component: AuthorizePhonePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AuthorizePhonePage]
})
export class AuthorizePhonePageModule {}

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoadingService } from "../../services/loading.service";
import { Storage } from "@ionic/storage";
import { DbStorageService } from "../../services/db-storage.service";

@Component({
  selector: "app-authorize-phone",
  templateUrl: "./authorize-phone.page.html",
  styleUrls: ["./authorize-phone.page.scss"]
})
export class AuthorizePhonePage implements OnInit {
  public idRegister = "";
  constructor(
    private router: Router,
    private loading: LoadingService,
    private storage: DbStorageService
  ) {}
  ionViewWillEnter() {
  
    this.loading.show();
   
    this.storage.getSerialAuthorize().subscribe(data => {
      this.idRegister = data;        
    });

    setTimeout(() => {     
      this.loading.hide();
    }, 1000);
  }
  ngOnInit() {
  
  }

  restartApp() {
    //window.location.reload();
    this.router.navigate(["/splash"]);
  }
}

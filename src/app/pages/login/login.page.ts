import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ApiHttpRequestService } from "../../services/api-http-request.service";
import { IUser, ICheckStatusTOken } from "src/app/interfaces/public";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { LoadingService } from "../../services/loading.service";
import { Observable } from "rxjs";
import { DbStorageService } from "../../services/db-storage.service";
import { FormGroup, NgForm } from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  @ViewChild(NgForm, { static: false }) private myForm: NgForm;
  constructor(
    private http: ApiHttpRequestService,
    private auth: AuthService,
    private router: Router,
    private loading: LoadingService,
    private storage: DbStorageService
  ) {}

  data = [];

  ngOnInit() {
    this.loading.show();
    //check user is login
    this.checkSessionOpen().subscribe(data => {
      if (data) {
        //if user is loggin open
        this.auth.execCheckToken().subscribe(
          data => {
            let dataToken: ICheckStatusTOken = data;

            if (dataToken.status == 0) {
              this.router.navigate(["/login"]);
            } else {
              //update Profile Company before go to home
              this.auth.updateProfileCompany().subscribe(data => {
                this.router.navigate(["/home"]);
              });
            }

            setTimeout(() => {
              this.loading.hide();
            }, 1000);
          },
          error => {
            //if no connect server in check token
            this.router.navigate(["/home"]);
            setTimeout(() => {
              this.loading.hide();
            }, 1000);
          }
        );
      } else {
        //if user not has loggin

        //moment Login Authorize
        // this.auth.logoutUser().subscribe(data=>{
        //
        this.router.navigate(["/login"]);
        // });

        setTimeout(() => {
          this.loading.hide();
        }, 1000);
      }
    });
  }

  checkUserLogin(user: string, password: string) {
    this.loading.show();

    let data3: IUser = {
      password: password,
      user: user,
      remember: true
    };

    this.auth.checkLoginUser(data3).subscribe(data => {
      console.log("Login Validate : " + data);
      if (data == 1) {
        ///usr ok
        setTimeout(() => {
          this.loading.hide();
          this.router.navigate(["/home"]);
          this.myForm.resetForm();
        }, 1000);
      } else if (data == 2) {
        //login error
        setTimeout(() => {
          this.loading.hide();
          this.showAlertFails("Datos Incorrectos ..");
          this.myForm.resetForm();
        }, 1000);
      } else if (data == 3) {
        //fails  coneccion
        setTimeout(() => {
          this.loading.hide();
          this.showAlertFails("Fallo Coneccion ..");
        }, 1000);
      }
    });
  }

  checkStatusToken(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.auth.execCheckToken().subscribe(data => {
        observe.next(data);
      });
    });

    return result;
  }

  checkSessionOpen(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.storage.getProfileUser().subscribe(data => {
        if (data) {
          observe.next(true);
        } else {
          observe.next(false);
        }
      });
    });

    return result;
  }

  login(f: NgForm) {
    //console.log(f.value);
    this.checkUserLogin(f.value.email, f.value.password);
  }

  showAlertFails(msg: string) {
    this.loading.toastShow(msg);
  }
}

import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DbStorageService } from '../../services/db-storage.service';
import { IProfileCompany, varStorageGlobal } from 'src/app/interfaces/public';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { LoadingService } from '../../services/loading.service';
import { File } from "@ionic-native/file/ngx";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { AuthService } from '../../services/auth.service';
const fileMime = 'application/vnd.android.package-archive';
const SD_DIRECTORY = 'file:///sdcard//';
const FOLDER_SOFTFINC = 'softfinc';
const FOLDER_DOWNLOAD = 'download';
const PATH_DOWNLOAD = SD_DIRECTORY + FOLDER_SOFTFINC + '//' + FOLDER_DOWNLOAD + '//';

@Component({
  selector: 'app-update-app',
  templateUrl: './update-app.page.html',
  styleUrls: ['./update-app.page.scss'],
})

export class UpdateAppPage implements OnInit {
  versionApp: string = "";
  versionAvalible: string = "";
  urlDownloadApp = "";


  constructor(private storage:DbStorageService,
    private file:File,
    private fileOpener: FileOpener,
    private loading:LoadingService,
    private transfer: FileTransfer,
    private auth:AuthService) { }

  ngOnInit() {


    this.storage.getProfileCompany().subscribe(data => {
      let profile: IProfileCompany = data;
      this.versionApp = varStorageGlobal.VERSION_APP;
      this.versionAvalible = profile.versionAppMovil;
      this.urlDownloadApp = profile.urlDownloadApp;
    })
  }
  download() {

    
    this.loading.showInfinity();
    const fileTransfer: FileTransferObject = this.transfer.create();


    
    this.storage.getProfileCompany().subscribe(data => {  

      let profile: IProfileCompany= data;
          
          // var as= JSON.parse(JSON.stringify(data));
          //  console.log(as);

            //http://10.10.0.101:20814/api/download/app/

       

      const url = profile.urlDownloadApp + profile.idCompany;
      console.log(url)  ;

      fileTransfer.download(url, PATH_DOWNLOAD + 'SoftFinc.apk').then((entry) => {
        // alert('download complete: ' + entry.toURL());

        this.fileOpener.open(entry.toURL(), fileMime)
          .then(() => {     

            //update profle Company 
            this.auth.updateProfileCompany().subscribe(data => {  
              this.loading.hide();
             }, error => {
              this.loading.hide();
            });
            
          })
          .catch(e => {
            this.loading.hide();
            alert("Error al Abrir archivo..");
          });

      }, (error) => {

        this.loading.hide();
        alert("Error Descargar archivo..");
      });



    }, error => {
      this.loading.hide();
      alert("Error IdCompany..");
    });



  }


  initDownload() {

    this.download();
    // this.http.downloadApp("demo key").subscribe(response =>{
    //   var filename="demo.apk"
    //   let dataType = response.type;
    //   let binaryData = [];
    //   binaryData.push(response);
    //   let downloadLink = document.createElement('a');
    //   downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
    //   if (filename)
    //       downloadLink.setAttribute('download', filename);
    //   document.body.appendChild(downloadLink);
    //   downloadLink.click();
    // },error=>{
    //   console.log(error);
    // })

  }
}

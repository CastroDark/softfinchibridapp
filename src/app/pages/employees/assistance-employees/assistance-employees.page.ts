import { Component, OnInit, ViewChild } from "@angular/core";
import { MenuController, ModalController, IonSearchbar } from "@ionic/angular";
import { ApiHttpRequestService } from "../../../services/api-http-request.service";
import { LoadingService } from "../../../services/loading.service";
import {
  IListShowPunchEmployee,
  IEmployee,
  IGroupEmployee,
  IOccupationEmployee
} from "src/app/interfaces/public";
import { Observable } from "rxjs";
import { Storage } from "@ionic/storage";
import { DbStorageService } from "../../../services/db-storage.service";
import { DomSanitizer } from "@angular/platform-browser";
import { FilesService } from "../../../services/files.service";
import { DetailsPunchEmployeeComponent } from "../../../components/modals/details-punch-employee/details-punch-employee.component";
import { IListImages } from "../../../interfaces/public";

@Component({
  selector: "app-assistance-employees",
  templateUrl: "./assistance-employees.page.html",
  styleUrls: ["./assistance-employees.page.scss"]
})
export class AssistanceEmployeesPage implements OnInit {
  @ViewChild("search", { static: false }) searchbar: IonSearchbar;

  isSearchOpened: boolean;
  inputSearchText: string;
  myDate: string = "";

  listLocaldb: Array<IListShowPunchEmployee> = [];

  dbEmployees: Array<IEmployee> = [];
  dbGroups: Array<IGroupEmployee> = [];
  dbOccupationsEmployees: Array<IOccupationEmployee> = [];
  dbListImg64: Array<IListImages> = [];

  groupSearch: number;
  sendShowAllGroup: number;
  dataShow: Observable<Array<IListShowPunchEmployee>>;
  countList: number;
  execSearch: boolean;

  constructor(
    private menuCtrl: MenuController,
    private http: ApiHttpRequestService,
    private loading: LoadingService,
    private storage: DbStorageService,
    public _DomSanitizer: DomSanitizer,
    private files: FilesService,
    private modalCtrl: ModalController
  ) {}

  ionViewWillEnter() {
    setTimeout(() => {
      this.menuCtrl.enable(false);
    }, 500);
  }

  ngOnInit() {
    this.sendShowAllGroup = 1;
    this.inputSearchText = "";
    this.countList = 0;
    this.execSearch = false;
  }

  getDataConfig(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      //get employees to database
      this.storage.getListAllEmployees().subscribe(async data => {
        this.dbEmployees = data;

        //get GroupsEmployees
        this.storage.getListAllGroupsEmployees().subscribe(async data => {
          this.dbGroups = data;
        });

        //get Occupations employee
        this.storage.getListAllOccupationEmployees().subscribe(async data => {
          this.dbOccupationsEmployees = data;
        });

        //get List Images Base64 Employee
        this.storage.getListALlImgBase64().subscribe(data => {
          this.dbListImg64 = data;
          observe.next(true);
        });
      });
    });

    return result;
  }

  //#region Managed Input Search
  showSearchBar(event) {
    this.isSearchOpened = true;
    setTimeout(() => {
      this.searchbar.setFocus();
    }, 100);
  }

  closeSearchBar(event) {
    this.isSearchOpened = false;
    this.inputSearchText = "";
  }

  inputSearchChanged(event) {
    console.log(event.target.value);

    //this.inputSearchText=event.target.value
    this.searchChanged("");
  }
  //#endregion

  //#region Filter assistance employee
  searchChanged(event) {
    this.loading.show();
    if (event != "") {
      this.inputSearchText = event.target.value;
    }

    setTimeout(() => {
      this.dataShow = this.filterAssistance();
      // console.log("paso");
    }, 800);
  }

  filterAssistance(): Observable<Array<IListShowPunchEmployee>> {
    const observe = new Observable<Array<IListShowPunchEmployee>>(observer => {
      //if (!this.localDataDBList) return [];
      // if (!this.localDataDBList) return this.dbList;

      this.inputSearchText = this.inputSearchText.toLowerCase();
      var get: Array<IListShowPunchEmployee>;
      if (this.groupSearch != 0) {
        get = this.listLocaldb
          .filter(item => item.idGroup === this.groupSearch)
          .filter(
            it =>
              it.nickName.toLowerCase().includes(this.inputSearchText) ||
              it.name.toLowerCase().includes(this.inputSearchText)
          );
      } else {
        get = this.listLocaldb.filter(
          it =>
            it.nickName.toLowerCase().includes(this.inputSearchText) ||
            it.name.toLowerCase().includes(this.inputSearchText)
        ); // only filter country name
      }

      this.countList = get.length;

      setTimeout(() => {
        this.loading.hide();
      }, 1000);

      this.execSearch = false;
      observer.next(get);
    });

    return observe;
  }

  //#endregion

  //#region Fill DataLocal

  getDataServer() {
    this.loading.show();
    this.http.checkStatusServer().subscribe(
      data => {
        this.http.GetReportAssistanceDate(this.myDate).subscribe(async data => {
          //get data of Server
          let listado: Array<ModelDataserver> = data.data;

          if (listado) {
            //clear data
            while (this.listLocaldb.length > 0) {
              this.listLocaldb.pop();
            }

            let i: number = 0;

            for (let entry of listado) {
              i++;

              //console.log(entry.IdEmployee); // 1, "string", false
              // var randomGroup2 = Math.floor(Math.random() * 3) + 1;
              //var imgName = "demo.jpg";
              let imgTransform = "";

              var employee: IEmployee = this.dbEmployees.filter(
                item => item.Id === entry.IdEmployee
              )[0];
              var group: IGroupEmployee = this.dbGroups.filter(
                i => i.Id === employee.IdGroup
              )[0];
              var occupationsEmplo: IOccupationEmployee = this.dbOccupationsEmployees.filter(
                x => x.Id == employee.IdOccupation
              )[0];

              if (this.dbListImg64) {
                var imgBase: IListImages = this.dbListImg64.filter(
                  x => x.name == employee.PathImg
                )[0];
                if (imgBase) {
                  imgTransform = imgBase.base64;
                } else {
                  imgTransform = this.files.img;
                }
              } else {
                imgTransform = this.files.img;
              }

              var insert: IListShowPunchEmployee = {
                ListGroup: entry.ListGroup,
                id: entry.IdEmployee,
                asigneDayWork: 0,
                idOccupation: 189,
                nameOccupation: occupationsEmplo.Name,
                name: employee.Name,
                idGroup: employee.IdGroup,
                nameGroup: group.Name,
                nickName: employee.NickName,
                urlImg: "",
                imgSanitizer: this._DomSanitizer.bypassSecurityTrustUrl(
                  imgTransform
                )
              };
              //Insert  to Datashow
              this.listLocaldb.push(insert);

              // await this.files
              //   .convertImgToBase64(employee.PathImg)
              //   .then(async _data => {
              //     imgTransform = _data;
              //     console.log(_data);

              //     // //test save base64 to server img
              //     // this.http
              //     //   .SendBase64ToSaveFileServer(_data, "11111", "")
              //     //   .subscribe(data => {
              //     //     console.log(data);
              //     //   });

              //     // if (i == 1) {

              //     //   // this.http.SendFilenameToGetBase64("11111", "demo22.jpg").subscribe(data => {
              //     //   //  console.log(data.msg);
              //     //   //  this.displayImage =  data.msg;
              //     //   // }, error => {

              //     //   // });

              //     // }
              //   })
              //   .catch(error => alert(error));

              //console.log(this.dbEmployees)

              //  console.log("Aqui: " +this.dbGroups);
            }
            //console.log(this.listLocaldb);
          }

          this.loading.hide();

          setTimeout(() => {
            this.searchChanged("");
          }, 500);
        });
      },
      error => {
        this.execSearch = false;
        this.loading.hide();
        this.loading.toastShow("No Hay Coneccion..");
      }
    );
  }

  //#endregion

  //#region "change Group"
  groupChange(event) {
    //console.log(event)
    this.groupSearch = Number(event);
    this.searchChanged("");
  }

  getIdGroupDefault(event) {
    //console.log(event);
    this.groupSearch = Number(event);
    //onsole.log(event)
    // this.groupSearch = Number(event);
    // this.searchChanged("");
  }

  //#endregion

  //#region "Managed Date"
  eventChangeDate(event) {
    this.myDate = event;
    //console.log(" data" + this.myDate);

    if (!this.execSearch) {
      this.execSearch = true;

      setTimeout(() => {
        this.getDataConfig().subscribe(data => {
          this.getDataServer();
        });
      }, 500);
    }
  }
  //#endregion

  //#region Select Item

  itemSelected(item) {
    //console.log(item);
    this.presentModal(item);
  }
  //#endregion

  //#region Manage Modal SHow details

  // async presentModal(item: any) {
  //   const modal = await this.modalCtrl.create({
  //     component: DetailsPunchEmployeeComponent
  //   });
  //   return await modal.present();
  // }
  async presentModal(item: any) {
    const modal = await this.modalCtrl.create({
      component: DetailsPunchEmployeeComponent,
      componentProps: {
        data: item
      }
    });
    return await modal.present();
  }
}

//#endregion

export interface ModelDataserver {
  IdEmployee: number;
  ListGroup: [
    {
      AssignedDayLabored: number;
      Date: string;
      IdCompany: string;
      IdEmployee: number;
      IdUserReader: number;
      Idkey: number;
      Note: string;
      PositionFinger: number;
    }
  ];
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AssistanceEmployeesPage } from './assistance-employees.page';
import { ComponentModule } from '../../../components/component.module';

const routes: Routes = [
  {
    path: '',
    component: AssistanceEmployeesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [AssistanceEmployeesPage]
})
export class AssistanceEmployeesPageModule {}

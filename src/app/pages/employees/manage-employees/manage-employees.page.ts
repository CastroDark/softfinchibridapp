import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener
} from "@angular/core";
import { MenuController, ModalController, IonSearchbar } from "@ionic/angular";
import { FormsEmployeesComponent } from "../../../components/modals/forms-employees/forms-employees.component";
import {
  IEmployee,
  IProfileGeneralEmployeeShow
} from "../../../interfaces/public";
import { Observable, BehaviorSubject } from "rxjs";
import { LoadingService } from "../../../services/loading.service";
import { DbStorageService } from "../../../services/db-storage.service";
import { IShowProfileEmployee } from "../../../interfaces/public";
import { ObserveOnMessage } from "rxjs/internal/operators/observeOn";

@Component({
  selector: "app-manage-employees",
  templateUrl: "./manage-employees.page.html",
  styleUrls: ["./manage-employees.page.scss"]
})
export class ManageEmployeesPage implements OnInit {
  @ViewChild("search", { static: false }) searchbar: IonSearchbar;

  isSearchOpened: boolean;
  modeEditForm: boolean = false;

  dbListLocalEmployees: Array<IProfileGeneralEmployeeShow> = [];
  dataShow: Array<IProfileGeneralEmployeeShow> = [];
  
  inputSearchText: string = "";
  groupSearch: number;
  statusSearch: number;
  sendShowAllGroup: number;
  sendShowAllStatus: number;
  countList: number = 0;
  countDb:number=0;

  filterActivate: boolean = false;
  private timer;

  constructor(
    private menuCtrl: MenuController,
    private modalCtrl: ModalController,
    private loading: LoadingService,
    private storage: DbStorageService
  ) {}

  // fillDbLocal(): Observable<any> {
  //   const result = new Observable<any>(observe => {});
  //   return result;
  // }

  oldCode() {
    // this.gelistEmployees().subscribe(data => {
    //   this.storage.getAllProfileShowEmployees(data).then(async data => {
    //     this.dbListLocalEmployees = await data;
    //     // let i: number = 0;
    //     // let f: number = this.dbListLocalEmployees.length;
    //     // console.log(i);
    //     // console.log(f);
    //     // while (i <= f) {
    //     //   // code block to be executed
    //     //   this.dbListLocalEmployees=data;
    //     //   i++;
    //     // }
    //     // console.log("acabo");
    //     setTimeout(async () => {
    //       observe.next(true);
    //     }, 500);
    //   });
    // });
    //observe.next(true);
    //  alert(this.dbListLocalEmployees.length);
    //    await this.storage.getAllProfileShowEmployees(listEmployees).subscribe( async data => {
    //           alert(this.dbListLocalEmployees.length);
    //            // let resu :Array<IProfileGeneralEmployeeShow>=await data
    //            this.dbListLocalEmployees = data;
    //            setTimeout(async () => {
    //              //console.log(data);
    //              //console.log(this.dbListLocalEmployees);
    //              observe.next(true);
    //            },8000);
    //         });
    //if (!this.localDataDBList) return [];
    // if (!this.localDataDBList) return this.dbList;
    // this.inputSearchText = this.inputSearchText.toLowerCase();
    // var get: Array<IProfileGeneralEmployeeShow>;
    // if (this.groupSearch != 0) {
    //   get = this.dbListLocalEmployees
    //     .filter(item => item.idGroup === this.groupSearch)
    //     .filter(
    //       it =>
    //         it.nickName.toLowerCase().includes(this.inputSearchText) ||
    //         it.name.toLowerCase().includes(this.inputSearchText)
    //     );
    //   switch (this.statusSearch) {
    //     case 0:
    //       break;2
    //     case 1:
    //       get = get.filter(x => x.status === 1);
    //       break;
    //     case 2:
    //       get = get.filter(x => x.status === 0);
    //       break;
    //     default:
    //       break;
    //   }
    // } else {
    //   get = this.dbListLocalEmployees.filter(
    //     it =>
    //       it.nickName.toLowerCase().includes(this.inputSearchText) ||
    //       it.name.toLowerCase().includes(this.inputSearchText)
    //   );
    //   switch (this.statusSearch) {
    //     case 0:
    //       break;
    //     case 1:
    //       get = get.filter(x => x.status === 1);
    //       break;
    //     case 2:
    //       get = get.filter(x => x.status === 0);
    //       break;
    //     default:
    //       break;
    //   }
    // }
    //   // console.log(get);
    //  // this.countList = get.length;
    //   setTimeout(() => {
    //     this.loading.hide();
    //   }, 1000);
    // this.storage.get
    //   this.filterActivate = false;
    //   //this.execSearch = false;
    //   observer.next(get);
  }

  // gelistEmployees(): Observable<number[]> {
  //   const result = new Observable<number[]>(observe => {
  //     let listEmployees: Array<number> = [];
  //     this.storage.getListAllEmployees().subscribe(async data => {
  //       for (let item of data) {
  //         listEmployees.push(item.Id);
  //       }
  //       setTimeout(() => {
  //         observe.next(listEmployees);
  //       }, 200);
  //     });
  //   });

  //   return result;
  // }

  ionViewWillEnter() {
    setTimeout(() => {
      this.menuCtrl.enable(false);
    }, 500);
  }

  ionViewWillLeave() {
   
    clearInterval(this.timer);
   
    //this.nav.navigateRoot(['/home']);
  }

  ngOnInit() {   
    this.sendShowAllGroup = 1;
    this.sendShowAllStatus = 0;

    // setTimeout(() => {
    //   this.loading.show();

    //   // this.fillDbLocal().subscribe(data => {
    //   //   setTimeout(() => {
    //   //     this.loading.hide();
    //   //     this.initSearch("");
    //   //   }, 300);
    //   // });
    //   setTimeout(() => {
    //     this.loading.hide();
    //     this.initSearch("");
    //   }, 300);
    // }, 500);

    setTimeout(() => {
      this.initSearch("");
    }, 800);
  }

 
  //#region managed Filter Employees

  async initSearch(event) {

    clearInterval(this.timer);
    if (!this.filterActivate) {
      this.loading.show();
      if (event != "") {
        this.inputSearchText = event.target.value;
      }
      this.filterEmployees().subscribe(data => {          
        this.timer = setInterval(async _ => {
            
          if(this.dataShow.length===this.countDb){
            clearInterval(this.timer);
             //console.log(this.dataShow.length);
          }
          this.dataShow = data;           
               
          this.countList=this.dataShow.length;        
        },200);      
        setTimeout(async () => { 
          this.countList = this.dataShow.length;
        
          setTimeout(() => {
            this.loading.hide();
          }, 100);
        },800);
      });

      // setTimeout(() => {
      //   this.filterEmployees();
      //   //this.dataShow = this.filterEmployees();
      //   // console.log("paso");
      // }, 800);
    }
    this.filterActivate = true;
  }

  filterEmployees(): Observable<Array<IProfileGeneralEmployeeShow>> {
    const observe = new Observable<Array<IProfileGeneralEmployeeShow>>(
      observer => {
        this.storage
          .getListAllEmployeesToStatusIdgroup(
            this.statusSearch,
            this.groupSearch,
            this.inputSearchText
          )
          .subscribe(async data => {
            //this.dataShow=data;

            if (data) {
              let employeesFound: Array<number> = [];
              for (let item of data) {
                await employeesFound.push(item.Id);
              }
              this.storage
                .getAllProfileShowEmployees(employeesFound)
                .then(data => {
                  
                  this.countDb=data.length;    
                  this.countList=data.length;              
                  ///   console.log(data);
                  observer.next(data);
                  this.filterActivate = false;
                });

              // })  subscribe( async data => {
              //         alert(this.dbListLocalEmployees.length);
              //          // let resu :Array<IProfileGeneralEmployeeShow>=await data
              //          this.dbListLocalEmployees = data;
              //          setTimeout(async () => {
              //            //console.log(data);
              //            //console.log(this.dbListLocalEmployees);
              //            observe.next(true);
              //          },8000);
            } else {
              //not data found
              // console.log("not found Data");
              observer.next((this.dataShow = []));
              this.filterActivate = false;
            }
          });
      }
    );

    return observe;
  }

 

  //#endregion

  //#region manage Search input
  showSearchBar($event) {
    this.isSearchOpened = true;
    //this.searchbarElem.nativeElement.setFocus();

    setTimeout(() => {
      this.searchbar.setFocus();
    }, 100);
  }

  dismissSearchBar($event) {
    this.isSearchOpened = false;
    this.inputSearchText = "";
  }

  inputSearchChanged($event) {
    //console.log(this.inputSearchText);
    this.initSearch("");
  }
  //#endregion

  //#region "change Group"
  groupChange(event) {
    //console.log(event)
    this.groupSearch = Number(event);
    this.initSearch("");
  }

  getIdGroupDefault(event) {
    //console.log(event);
    this.groupSearch = Number(event);
    //this.initSearch("");
  }

  statusChange(event) {
    //console.log(event)
    this.statusSearch = Number(event);
    this.initSearch("");
  }

  getIdStatusDefault(event) {
    //console.log(event);
    this.statusSearch = Number(event);
  }

  //#endregion

  //#region Smodal Edit or add
  showModalFormsAdd() {
    this.modeEditForm = false;
    this.presentModal(0);
  }
  closeModalForms() {}

  showEditProfile(idEmployee) {
    this.modeEditForm = true;
    setTimeout(() => {
      this.presentModal(idEmployee);
    }, 0);

    // this.storage.getEmployeeToId(event).subscribe(data=>{
    //   console.log(data);
    // })
  }

  async presentModal(idEmployee: number) {
    const modal = await this.modalCtrl.create({
      component: FormsEmployeesComponent,
      componentProps: {
        data: idEmployee
      }
    });
    modal.onDidDismiss().then(data => {
      this.closeModal(data);
    });

    return await modal.present();
  }

  closeModal(event) {
    //console.log(event);
    if (event.data.dismissed) {     
      // this.fillDbLocal().subscribe(data => {
      setTimeout(() => {       
        this.initSearch("");
      }, 300);
      // });
    }
  }

  //#endregion
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ManageEmployeesPage } from './manage-employees.page';
import { ComponentModule } from '../../../components/component.module';

const routes: Routes = [
  {
    path: '',
    component: ManageEmployeesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [ManageEmployeesPage]
})
export class ManageEmployeesPageModule {}

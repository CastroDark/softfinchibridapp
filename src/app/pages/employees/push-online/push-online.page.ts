import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter
} from "@angular/core";
import {
  MenuController,
  ModalController,
  Events,
  NavController
} from "@ionic/angular";
import { SignalRService } from "src/app/services/signal-r.service";
import { DbStorageService } from "../../../services/db-storage.service";
import { IPushEmployeeOnline } from "src/app/interfaces/public";
import { IShowProfileEmployee } from "../../../interfaces/public";
import { ProfileEmployeeComponent } from "../../../components/modals/profile-employee/profile-employee.component";
import { Subject, Observable, observable } from "rxjs";

@Component({
  selector: "app-push-online",
  templateUrl: "./push-online.page.html",
  styleUrls: ["./push-online.page.scss"]
})
export class PushOnlinePage implements OnInit {
  detailsEmployee: IPushEmployeeOnline;
  detailsEmployeeShow: IShowProfileEmployee;
  modalIsShow: boolean = false;
  componentMethodCalled$: any;
  bSubscription: any;

  private timer;
  constructor(
    private menu: MenuController,
    private storage: DbStorageService,
    private signalR: SignalRService,
    private modalCtrl: ModalController,
    public events: Events,
    private nav: NavController
  ) {
    // // create observable
    // this.signalR.componentMethodCalled$.subscribe(data => {
    //   this.callEvent(data);
    // });

    this.bSubscription = this.signalR.componentMethodCalled$.subscribe(data => {
      this.callEvent(data).subscribe(data => {
        console.log(data);
      });
    });
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.menu.enable(false);
    }, 300);
    this.modalIsShow = false;
  }

  ionViewWillLeave() {
    // close page end conecctions socker signalr
    this.signalR.endConnection().then(data => {
      //console.log(data);
    });
    clearInterval(this.timer);
    window.location.reload();

    //this.nav.navigateRoot(['/home']);
  }

  ngOnInit() {
    this.signalR.startConnection().then(data => {
      this.signalR.addSuscribeToNotifiPushOnline();
      // this.signalR.callAddUser();
    });

    this.timer = setInterval(_ => {
      this.signalR.statusConecction().then(
        data => {
          if (!data) {
            console.log("desconectado");
           this.nav.navigateRoot(['/home']);

            // this.signalR.endConnection().then(data => {
            //   this.signalR.startConnection().then(data => {
            //     this.signalR.addSuscribeToNotifiPushOnline();
            //   });
            // });
          } else {
            console.log("conectado");
          }
        },
        error => {
          console.log("Error");
        }
      );

      // this.signalR
      //   .statusConecction()
      //   .then(data => {
      //     if (!data) {
      //       console.log("desconectado");
      //       this.signalR.startConnection().then(data => {
      //         this.signalR.addSuscribeToNotifiPushOnline();
      //       });
      //     } else {
      //       console.log("conectado");
      //     }
      //   })
      //   .catch(error => {
      //     // console.log(error);
      //   });
    }, 10000);

    // this.autoSaveInterval = setInterval(function() {

    // }.bind(this), 5000);

    // this.autoSaveInterval;
  }

  callEvent(data): Observable<any> {
    const result = new Observable<any>(observe => {
      // console.log(data);
      this.detailsEmployee = data;

      this.storage
        .getEmployeeProfileToId(this.detailsEmployee.idEmployee)
        .subscribe(data => {
          this.detailsEmployeeShow = data;
          this.presentModal();
          observe.next(data);
        });

     
    });

    return result;

    //this.modalIsShow = true;

    // setTimeout(() => {
    //   this.modalIsShow = false;
    // }, 5000);
    // console.log(value);
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: ProfileEmployeeComponent,
      componentProps: {
        data: this.detailsEmployeeShow
      }
    });
    return await modal.present();
  }

  ejecuteCalback(employee: IPushEmployeeOnline) {
    // console.log("hola");
  }
}

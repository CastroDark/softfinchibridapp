import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PushOnlinePage } from './push-online.page';
import { ComponentModule } from '../../../components/component.module';

const routes: Routes = [
  {
    path: '',
    component: PushOnlinePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,   
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [PushOnlinePage]
})
export class PushOnlinePageModule {}

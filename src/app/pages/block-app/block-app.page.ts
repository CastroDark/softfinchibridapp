import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-block-app',
  templateUrl: './block-app.page.html',
  styleUrls: ['./block-app.page.scss'],
})
export class BlockAppPage implements OnInit {

  constructor(private router:Router,
    private menu:MenuController) { }

    ionViewWillEnter() {
      this.menu.enable(false);
    }

  ngOnInit() {
  }


  restartApp(){
    //window.location.reload();
   this.router.navigate(['/splash']);
  }

}

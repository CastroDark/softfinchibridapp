import { Component, OnInit } from "@angular/core";
import { MenuController, LoadingController } from "@ionic/angular";
import { ApiHttpRequestService } from "../../services/api-http-request.service";
import {
  IEmployee,
  IGroupEmployee,
  ICheckStatusTOken,
  IListImages,
  IUpdateDataApp,
  ICurrency
} from "src/app/interfaces/public";
import { Storage } from "@ionic/storage";
import { DbStorageService } from "src/app/services/db-storage.service";
import { FilesService } from "src/app/services/files.service";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import {
  IProfilePhone,
  IProfileCompany,
  varStorageGlobal
} from "../../interfaces/public";
import { Observable } from "rxjs";
import { resolve, reject } from "q";
import "rxjs/add/observable/forkJoin";
import { SignalRService } from "../../services/signal-r.service";
import { LoadingService } from "../../services/loading.service";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { IPayRoll } from "../../interfaces/public";
import { DatetimeService } from "../../services/datetime.service";
import { delay } from "rxjs/operators";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"]
})
export class HomePage implements OnInit {
  recorrer: boolean;
  idDevice: number;
  autoSaveInterval;
  constructor(
    private menu: MenuController,
    private http: ApiHttpRequestService,
    private storage: DbStorageService,
    private files: FilesService,
    private auth: AuthService,
    private router: Router,
    private signalR: SignalRService,
    private loading: LoadingService,
    private time: DatetimeService
  ) {}

  ionViewWillEnter() {
    this.checkDataAppExist().subscribe(data => {
      if (data) {
        //need update data or dot not nedd update
        this.checkUpdateApp().then(bool => {
          if (bool) {
            // console.log("update app");
            //get profileCompany to Refres update app
            setTimeout(() => {
              this.router.navigate(["update-app"]);
            }, 50);

          } else {
            this.menu.enable(true);
            this.checkTokenStatus();
            setTimeout(() => {
              this.updateDb();
            },500);
          }
        });
      } else {
        //fails ON Change update DataApp
        this.router.navigate(["splash"]);
      }
    });
  }

  async ngOnInit() {
    // var i = 0;

    // while (i <= 50) {
    //   var dat = new Date();
    //   this.time.getUniqueId().subscribe(data => {
    //     console.log(data);
    //   });
      
    //   i++;
    // }

    // const connection = new signalR.HubConnectionBuilder()
    //   .configureLogging(signalR.LogLevel.Information)
    //   .withUrl("http://10.10.0.101:20814/finger")
    //   .build();
    // connection.start().then(function () {
    //   console.log('Connected!');
    // }).catch(function (err) {
    //   return console.error(err.toString());
    // });
    // connection.send("Send","message")
    // connection.on("Send", (message: string) => {
    //   //this.messageService.add({ severity: type, summary: payload, detail: 'Via SignalR' });
    // });
    // connection.on("BroadcastMessage", (type: string, payload: string) => {
    //   this.messageService.add({ severity: type, summary: payload, detail: 'Via SignalR' });
    // });
  }

  checkConnecction() {}

  // private startHttpRequest = () => {
  //   this.http.get('https://localhost:5001/api/chart')
  //     .subscribe(res => {
  //       console.log(res);
  //     })
  // }

  checkTokenStatus() {
    this.auth.execCheckToken().subscribe(
      data => {
        let dataToken: ICheckStatusTOken = data;
        if (dataToken.status == 0) {
          this.router.navigate(["/login"]);
        } else {
          // this.router.navigate(["/home"]);
        }
        setTimeout(() => {
          // this.loading.hide();
        }, 1000);
      },
      error => {
        //console.log("Error chek token ")
        //if no connect server
        //this.router.navigate(["/home"]);
        //setTimeout(() => {
        // this.loading.hide();
        //},1000);
      }
    );
  }

  send(event) {
    this.signalR.Send("Texto Largo Para ,,,demoo");
  }

  updateDb() {
    //check server updata Data App
    this.http.checkStatusServer().subscribe(
      data => {
        this.storage.getProfilePhone().subscribe(data => {
          let profile: IProfilePhone = data;
          this.http.GetIfUpdateDataApp(profile.idPhone).subscribe(
            data => {
              let serverUpdate: IUpdateDataApp = data;
              if (serverUpdate) {
                this.updateDataDeviceToServer(serverUpdate);
              }
            },
            error => {
              //console.log(""error);
            }
          );
        });
      },
      error => {
        //fails test server
        console.log("error=> " + error);
      }
    );
  }

  updateDataDeviceToServer(dataUpdate: IUpdateDataApp) {
    this.idDevice = dataUpdate.IdDevice;

    if (dataUpdate.Config) {
      this.auth.updateProfileCompany().subscribe(data => {
        this.sendInfoUpdateDataFinishFieldApp(this.idDevice, "Config", "false");
      });
    }

    setTimeout(() => {
      this.getOthers(dataUpdate.Others).then(data => {});
    }, 4000);

    setTimeout(() => {
      if (dataUpdate.Employees) {
        this.getEmployee(dataUpdate.Employees);
      }
    }, 7000);

    setTimeout(() => {
      if (dataUpdate.ImgEmployees) {
        let timeout: number = 0;
        let count = 0;
        this.storage.getListAllGroupsEmployees().subscribe(data => {
          if (data) {
            let listGet: Array<IGroupEmployee> = data;
            var listGroup: Array<string> = [];
            for (let group of listGet) {
              listGroup.push(String(group.Id));
            }

            for (let item of listGroup) {
              count++;
              timeout += 1000;
              if (count === item.length) {
                //console.log(count);
                this.getImgEmplo(item, true);
              } else {
                //console.log("freno");
                this.getImgEmplo(item, false);
              }
              // setTimeout(() => {

              // }, timeout);
            }
          }
        });
      }
    }, 9000);
  }

  getOthers(value: boolean): Promise<any> {
    const result = new Promise((resolve, reject) => {
      if (value) {
        //get groups employees
        this.http.GetListGroupsEmployees().subscribe(data => {
          let list: Array<IGroupEmployee> = data.data;
          this.storage.saveDataGroupsEmployees(list);

          //get occupations  employees
          this.http.GetListOccupationEmployees().subscribe(data => {
            let list: Array<IGroupEmployee> = data.data;
            this.storage.saveDataOccupationEmployees(list);

            this.http.GetListCurrencys().subscribe(data => {
              let list: Array<ICurrency> = data.data;
              this.storage.saveListCurrency(list);

              this.http.GetListPaysRoll().subscribe(data => {
                let list: Array<IPayRoll> = data.data;
                this.storage.saveListPayRoll(list);

                //Comfirm Update All
                setTimeout(() => {
                  this.sendInfoUpdateDataFinishFieldApp(
                    this.idDevice,
                    "Others",
                    "false"
                  );
                  resolve(true);
                }, 500);
              });
            });

            // setTimeout(() => {
            //   this.DataBaseStorage.getOccupationEmployeeToId(10001).subscribe(data=>{
            //     console.log(data);
            //             });
            // },3000);
          });

          // setTimeout(() => {
          //   this.DataBaseStorage.getGroupEmployeeToId(11111).subscribe(data=>{
          //     console.log(data);
          //             });
          // },3000);
        });
      }
    });

    return result;
  }

  getEmployee(value: boolean) {
    //get Employees
    this.http.GetListEmployeeServer("2019-09-10").subscribe(data => {
      //console.log(data);
      let list: Array<IEmployee> = data.data;
      this.storage.saveDataEMployees(list);

      setTimeout(() => {
        this.sendInfoUpdateDataFinishFieldApp(
          this.idDevice,
          "Employees",
          "false"
        );
      }, 500);
    });
  }

  getImgEmplo(item: string, reportFinish: boolean) {
    let timeOut: number = 0;
    this.http.GetImgBase64AllEmployeesForIdGroup(item).subscribe(
      data => {
        console.log(data);
        let listBase64: Array<IGetAllImgEmployeeBase64> = data.data;
        let listImgBase64Employee: Array<IListImages> = [];
        for (let item of listBase64) {
          var imgEmployee: IListImages = {
            name: item.nameImg,
            base64: item.imgBase64
          };

          listImgBase64Employee.push(imgEmployee);
          //   setTimeout(() => {

          //   //this.files.saveBase64(item.imgBase64, item.nameImg).then(data => {
          //   //  console.log(data);
          //   // });
          // }, 1000);
        }

        this.storage.saveListImageBase64Employees(listImgBase64Employee);

        if (reportFinish) {
          setTimeout(() => {
            this.sendInfoUpdateDataFinishFieldApp(
              this.idDevice,
              "ImgEmployees",
              "false"
            );
          }, 500);
        }
        //console.log(listImgBase64Employee);
        //console.log(data);
      },
      error => {
        //fails petitions http
        console.log(error);
      }
    );
  }

  sendInfoUpdateDataFinishFieldApp(
    idDevice: number,
    field: string,
    value: string
  ) {
    this.http
      .SendInfoUpdateDataFinishApp(idDevice, field, value)
      .subscribe(data => {
        console.log(data);
      });
  }

  checkUpdateApp(): Promise<boolean> {
    const result = new Promise<boolean>((resolve, reject) => {
      this.storage.getProfileCompany().subscribe(data => {
        if (data) {
          let profile: IProfileCompany = data;
          if (profile.versionAppMovil == varStorageGlobal.VERSION_APP) {
            resolve(false);
          } else {
            resolve(true);
          }
        } else {
          reject(false);
        }
      });
    });

    return result;
  }

  //#region Check if App Need getAll data Config

  checkDataAppExist(): Observable<boolean> {
    const result = new Observable<boolean>(Observer => {
      this.storage.getListAllEmployees().subscribe(data => {
        if (data) {
          //not need get data Server
          // console.log("no necesita");
          Observer.next(true);
        } else {
          //console.log("si necesita");
          //if need Get Data of Server
          this.storage.getProfilePhone().subscribe(data => {
            let profilePhone: IProfilePhone = data;
            this.http.SendUpdateDataApp(String(profilePhone.idPhone)).subscribe(
              data => {
                console.log(data);
                Observer.next(true);
              },
              error => {
                //if fails in cahnge Update APp Server
                Observer.next(false);
              }
            );
          });
        }
      });
    });
    return result;
  }

  //#endregion
}

export interface IGetAllImgEmployeeBase64 {
  idEmployee: number;
  imgBase64: string;
  nameImg: string;
}

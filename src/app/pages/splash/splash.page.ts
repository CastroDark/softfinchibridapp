import { Component, OnInit } from "@angular/core";
import { HomePage } from "../home/home.page";
import { Router } from "@angular/router";
import { DbStorageService } from "../../services/db-storage.service";
import { AlertController, Platform } from "@ionic/angular";

import { ApiHttpRequestService } from "../../services/api-http-request.service";
import {
  varStorageGlobal,
  IRegisterPhone,
  IProfilePhone
} from "src/app/interfaces/public";
import { UniqueDeviceID } from "@ionic-native/unique-device-id/ngx";
import { Observable } from "rxjs";
import { FilesService } from "../../services/files.service";


@Component({
  selector: "app-splash",
  templateUrl: "./splash.page.html",
  styleUrls: ["./splash.page.scss"]
})
export class SplashPage implements OnInit {
  splash = true;
  imePhone: string = "";
  uidPhone: string = "";
  registerPhone: IRegisterPhone;
  //secondPage = HomePage;
  constructor(
    private router: Router,
    private storage: DbStorageService,
    private alertCtr: AlertController,
    private http: ApiHttpRequestService,
    private uniqueDeviceID: UniqueDeviceID,
    private files: FilesService,
    private plt: Platform
  ) {}

  ionViewWillEnter() {
    this.splash = true;

    this.getUidApp().subscribe(data => {
      this.registerPhone = {
        ime: this.imePhone,
        uidApp: this.uidPhone,
        versionApp: varStorageGlobal.VERSION_APP
      };

      this.checkUrlApi();
    });
  }

  ngOnInit() {
    if (this.plt.is("cordova")) {
      setTimeout(() => {
        this.files.CreateDirectories().then(data => {
          this.files.createUserpng().then(data => {});
        });
      }, 3000);
    }
  }

  //#region Manage UrlWEbApi
  checkUrlApi() {
    this.storage.GetUrlApi().subscribe(
      data => {
        if (!data) {
          this.sendApiUrl();
        } else {
          this.showLogin();
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  async sendApiUrl() {
    const alert = await this.alertCtr.create({
      header: "Inserte Direccion..",
      inputs: [
        {
          name: "url",
          type: "text",
          value: "http://10.10.0.101:20814",
          placeholder: "Ej: http://apiweb/api"
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            window.location.reload();
          }
        },
        {
          text: "Guardar",
          handler: data => {
            this.http.CheckUrlApi(data.url).subscribe(
              data2 => {
                if (data2.result) {
                  this.storage.saveUrlApi(data.url).subscribe(data => {
                    window.location.reload();
                  });
                }
              },
              error => {
                this.alertFailure();
              }
            );
          }
        }
      ]
    });

    await alert.present();
  }

  //#endregion

  showLogin() {
    this.checkAuthorizeApp().subscribe(data => {
      if (data == 1) {
        console.log("Start App");
        this.saveProfilePhoneDb().subscribe(data => {
          //console.log(data);
          let profile: IProfilePhone = data;
          setTimeout(() => {
            if (profile.status == 1) {
              //start app
              this.router.navigate(["/login"]);
            } else {
              //block App
              this.router.navigate(["/block-app"]);
            }
          }, 1500);
        });
      } else if (data == 2) {
        //phone unauthorized

        this.splash = false;
        this.router.navigate(["/authorize-phone"]);
      } else if (data == 0) {
        //
        //check if in Db Exist COnfig Profile phone
        //console.log("No coneccion");
        this.checkProfilePhoneDb().subscribe(data => {
          setTimeout(() => {
            if (data) {
              //console.log(data);
              let profile: IProfilePhone = data;
              if (profile.status == 1) {
                //start app
                this.router.navigate(["/login"]);
              } else {
                //block app
                this.splash = false;
                this.router.navigate(["/block-app"]);
              }
            } else {
              this.splash = false;
              this.router.navigate(["/authorize-phone"]);
            }
          }, 1500);
        });
      }
    });
  }

  async alertFailure() {
    const alert = await this.alertCtr.create({
      header: "Servidor no Responde.",
      buttons: [
        {
          text: "Reintentar.",
          handler: () => {
            this.sendApiUrl();
          }
        }
      ]
    });
    await alert.present();
  }

  //#region MANAGED STATUS AND PROFILE PHONE
  getUidApp(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.uniqueDeviceID
        .get()
        .then((uuid: any) => {
          console.log(uuid);
          this.uidPhone = uuid;
          this.imePhone = "877723811";
        })
        .catch((error: any) => {
          this.uidPhone = "a468c642-c2a5-11e9-9cb5-2a2ae2dbcce4";
          this.imePhone = "209981222224";
        });

      setTimeout(() => {
        observe.next(true);
      }, 2000);
    });

    return result;
  }

  checkAuthorizeApp(): Observable<number> {
    const result = new Observable<number>(observe => {
      this.http.CheckAutorizeApp(this.registerPhone).subscribe(
        data => {
          if (data.result) {
            observe.next(1);
          } else {
            this.storage.saveSerialAuthorize(data.msg).subscribe(data => {
              observe.next(2);
            });
          }
        },
        error => {
          //on fail check Profile Phone to continue
          observe.next(0);
        }
      );
    });

    return result;

    // this.storage.get(varStorageGlobal.PROFILEPHONE).then(data => {
    //   if (data) {
    //     let profilePhone: IProfilePhone = data;
    //     if (profilePhone.status == 1) {
    //       this.router.navigate(['/login']);
    //     } else {
    //       setTimeout(() => {
    //         this.DbStorage.LogoutDb().subscribe(data => {
    //           //console.log(data);
    //           this.router.navigate(['/register-app']);
    //         });
    //       }, 50);
    //     }
    //   } else {
    //     this.router.navigate(['/register-app']);
    //   }
    // });
  }

  saveProfilePhoneDb(): Observable<any> {
    const result = new Observable<any>(observe => {
      // this.loading.show();
      this.http.GetProfilePhone(this.registerPhone).subscribe(
        data => {
          this.storage.saveProfilePhone(data).subscribe(data => {
            observe.next(data);
          });
        },
        error => {
          observe.next(0);
        }
      );
    });

    return result;
  }

  checkProfilePhoneDb(): Observable<any> {
    const result = new Observable<any>(observe => {
      // this.loading.show();
      this.storage.getProfilePhone().subscribe(
        data => {
          observe.next(data);
        },
        error => {
          observe.error(null);
        }
      );
    });

    return result;
  }

  //#endregion
}

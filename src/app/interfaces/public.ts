export const varStorageGlobal = {
  VERSION_APP: "0.0.10",
  TOKEN: "TOKEN-AUTHORIZE",
  USER_ONLINE: "USER-ONLINE",
  PROFILE_COMPANY: "PROFILE-COMPANY",
  URL_API: "URL-API",
  IDREGISTERPHONE: "IDREGISTERPHONE",
  PROFILEPHONE: "PROFILE_PHONE",
  DB_EMPLOYEES: "DBEMPLOYEES",
  DB_GROUPSEMPLOYEES: "DBGROUPSEMPLOYEES",
  DB_OCCUPATIONEMPLOYEES: "DBOCCUPATIONSEMPLOYEES",
  DB_LISTIMAGES: "DBLISTIMAGESEMPL",
  DB_CURRENCYS: "DBCURRENCYS",
  DB_PAYROLL: "DBPAYROLL", 
};



export interface IResultModelBasic {
  result: boolean;
  msg: string;
}

export interface IGetToken {
  expiration: string;
  statusUser: number;
  token: string;
}

export interface IProfilePhone {
  idPhone: number;
  name: string;
  updateData: boolean;
  status: number;
}

export interface IRegisterPhone {
  uidApp: string;
  ime: string;
  versionApp: string;
}

export interface IProfilePhone {
  idPhone: number;
  name: string;
  updateData: boolean;
  status: number;
}

export interface IUser {
  user: string;
  password: string;
  remember: boolean;
}

export interface IGetToken {
  expiration: string;
  statusUser: number;
  token: string;
}

export interface IUserOnline {
  id: number;
  nickName: string;
  name: string;
  lastName: string;
  idCompany: string;
  status: number;
  lastLogin: string;
  idRol: number;
  urlImg: string;
  note: string;
  idSecret: string;
}

export interface IProfileCompany {
  idCompany: string;
  longName: string;
  shortName: string;
  slogan: string;
  phone: string;
  address: string;
  rnc: string;
  status: number;
  versionAppMovil: string;
  versionAppDesktop: string;
  urlDownloadApp: string;
  urlCloudDownloadApp: string;
}

export interface IGetToken {
  expiration: string;
  statusUser: number;
  token: string;
}

export interface ICheckStatusTOken {
  status: number;
  newToken: string;
  expiration: string;
}

export interface ISendDataRequestBasic {
  query1: string;
  query2: string;
  date1: string;
  date2: string;
}

export interface IEmployee {
  Address: string;
  Base64: string;
  BirthDate: string;
  Contacts: string;
  DateInsert: string;
  DocumentEmployee: string;
  Email: string;
  ExistFingerPrint: boolean;
  Id: number;
  IdCompany: string;
  IdCurrency: number;
  IdGroup: number;
  IdModeCheckAssist: number;
  IdNationality: number;
  IdOccupation: number;
  IdRoster: number;
  IdSegurityPolicy: number;
  IdWorkDay: number;
  Idkey: number;
  IgnoreWorkContract: boolean;
  LastName: string;
  Name: string;
  NickName: string;
  Note: string;
  PathImg: string;
  Status: number;
}

export interface IGroupEmployee {
  IdKey: number;
  Id: number;
  Name: string;
  Note: string;
  Status: number;
  IdCompany: string;
}

export interface IOccupationEmployee {
  IdKey: number;
  Id: number;
  Name: string;
  Note: string;
  Status: number;
  IdCompany: string;
}

export interface IListShowPunchEmployee {
  id: number;
  name: String;
  nickName: String;
  idOccupation: number;
  nameOccupation: string;
  idGroup: number;
  nameGroup: string;
  urlImg: string;
  imgSanitizer: any;
  asigneDayWork: number;
  ListGroup: [
    {
      AssignedDayLabored: number;
      Date: string;
      IdCompany: string;
      IdEmployee: number;
      IdUserReader: number;
      Idkey: number;
      Note: string;
      PositionFinger: number;
    }
  ];
}

export interface IListImages {
  name: string;
  base64: string;
}

export interface IUpdateDataApp {
  Idkey: number;
  IdDevice: number;
  Config: boolean;
  Others: boolean;
  Employees: boolean;
  ImgEmployees;
}

export interface IPushEmployeeOnline {
  idCompany: string;
  idEmployee: number;
  idUserRead: number;
  note: string;
  positionFinger: number;
  idActionPush: number;
}

export interface IShowProfileEmployee {
  nickName: string;
  name: string;
  image: any;
  nameGroup: string;
  contacts: string;
}

export interface IDevicePunch {
  Automatic: boolean;
  Id: number;
  IdCompany: string;
  IdKey: number;
  Mode: number;
  Name: string;
  Note: string;
  Restart: boolean;
  Status: number;
}

export interface ICurrency {
  Id: number;
  IdCompany: string;
  Idkey: number;
  Name: string;
  Note: string;
  Status: number;
  Symbol: string;
}

export interface IPayRoll {
  ExecDay: number;
  Id: number;
  IdCompany:string;
  Idkey: number;
  LastExec: string;
  Name: string;
  Note: string;
  Status: number;
}



export interface IProfileGeneralEmployeeShow {
  id: number;
  nickName: string;
  name: string;
  lastName: string;
  document: string;
  contacts: string;
  address: string;
  birthDate: string;
  idCompany: string;
  note: string;
  status: number;
  groupName: string;
  currency: string;
  roster: string;
  occupation: string;
  nationality: string;
  email: string;
  dateCreate: string;
  segurityPolicy: string;
  modeCheckAssistance: string;
  ignoreWorkContract: boolean;
  existFingerPrint: boolean;
  wordDays: string;
  idGroup:number,
  image:any;
}

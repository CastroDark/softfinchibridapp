import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-select-mode-device',
  templateUrl: './select-mode-device.component.html',
  styleUrls: ['./select-mode-device.component.scss'],
})
export class SelectModeDeviceComponent implements OnInit {
  @Input() defaultMode:string ;
  @Input() nameSelect:string;


  idDefault: string;
  listMode: any[] = [
    {
      id: 1,
      name: 'Asistencia',
      last: 'Smith',
    },
    {
      id: 2,
      name: 'Pago',
      last: 'Davis',
    },
    {
      id: 3,
      name: 'Perfil',
      last: 'Rosenburg',
    },
    {
      id: 4,
      name: 'Ingreso',
      last: 'Rosenburg',
    }
  ];

  constructor() { }

  ngOnInit() {

  this.idDefault=this.defaultMode;

    console.log(this.defaultMode);
    console.log(this.nameSelect)
  }


  changeValue(event){
    console.log(event)
  }
}

import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DatetimeService } from '../../services/datetime.service';

@Component({
  selector: 'app-datetime',
  templateUrl: './datetime.component.html',
  styleUrls: ['./datetime.component.scss'],
})
export class DatetimeComponent implements OnInit {  
  //@ViewChild('datePicker') datePicker;

  @Input() getDate: string;
  @Input() reciboIdCuenta: string;
  @Output() emitChangeDate = new EventEmitter();

  myDate = new Date().toISOString();
  customDayShortNames = ['s\u00f8n', 'man', 'tir', 'ons', 'tor', 'fre', 'l\u00f8r'];

  constructor(private moment: DatetimeService) { }

  ngOnInit() {


    this.moment.getDatetime().subscribe(date => {
      this.myDate = date;
      //console.log(this.myDate)
      this.moment.getFormatDate(date).subscribe(date => {
        //console.log(" 2 => " + date);
      });
    });

    //send update Date
    this.emitChangeDate.emit(this.myDate);
    //console.log(this.myDate);
    // // this.datePicker.target.value=this.myDate;
    // this.moment.getFormatDateTime("2019-08-15T06:47:42.134779").subscribe(data => {

    //   console.log(data)

    // })


  }


  onDateChange($event) {
    //console.log(this.myDate);
    this.emitChangeDate.emit(this.myDate);
    //this.fillDbList();
  }




}

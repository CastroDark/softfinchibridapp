import { Component, OnInit, Input } from "@angular/core";
import { IListShowPunchEmployee } from "src/app/interfaces/public";
import { NavParams, ModalController } from '@ionic/angular';


@Component({
  selector: "app-details-punch-employee",
  templateUrl: "./details-punch-employee.component.html",
  styleUrls: ["./details-punch-employee.component.scss"]
})
export class DetailsPunchEmployeeComponent implements OnInit {
  @Input() data: IListShowPunchEmployee;

  dataDetails:IListShowPunchEmployee;
  constructor(navParams: NavParams,private modalCtrl:ModalController) {
   // console.log(navParams.get("data"));
   //this.dataDetails=navParams.get("data");
  }

  ngOnInit() {}


  dismissModal(){ 
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }




}

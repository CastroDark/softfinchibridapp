import { Component, OnInit, Input } from "@angular/core";
import { IShowProfileEmployee } from "../../../interfaces/public";
import { NavParams, ModalController } from "@ionic/angular";

@Component({
  selector: "app-profile-employee",
  templateUrl: "./profile-employee.component.html",
  styleUrls: ["./profile-employee.component.scss"]
})
export class ProfileEmployeeComponent implements OnInit {
  @Input() data: IShowProfileEmployee;
  constructor(navParams: NavParams, private modalCtrl: ModalController) {}

  ngOnInit() {

   // console.log(this.data);

    setTimeout(() => {
      this.dismissModal()
    },6000);
  }

  dismissModal(){ 
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}

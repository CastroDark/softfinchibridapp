import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NavController, ModalController, NavParams } from "@ionic/angular";
import { AgeValidator } from "../../validators/age";
import { UsernameValidator } from "../../validators/username";
import { LoadingService } from "../../../services/loading.service";
import { IEmployee, IResultModelBasic } from "../../../interfaces/public";
import { ApiHttpRequestService } from "../../../services/api-http-request.service";
import { Storage } from "@ionic/storage";
import { DbStorageService } from "../../../services/db-storage.service";
import { Subject } from "rxjs";
@Component({
  selector: "app-forms-employees",
  templateUrl: "./forms-employees.component.html",
  styleUrls: ["./forms-employees.component.scss"]
})
export class FormsEmployeesComponent implements OnInit {
  @ViewChild("signupSlider", { static: false }) signupSlider;
  public slideOneForm: FormGroup;
  public slideTwoForm: FormGroup;

  idEmployee: number;
  public submitAttempt: boolean = false;
  public modeText = "";
  editEmployee: IEmployee;

  idGroup: string = "";
  idCurrency: string = "";
  idOcupation: string = "";
  idPayRoll: string = "";
  toggleStatus;
  hasChange: boolean = false;

  idNationality;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    private modalCtrl: ModalController,
    private loading: LoadingService,
    private http: ApiHttpRequestService,
    private navParams: NavParams,
    private storage: DbStorageService
  ) {}

  ngOnInit() {
    if (this.navParams.get("data") == 0) {
      //new
      this.idEmployee = 0;
      //this.modeText = " Crear Nuevo.";
      this.createNewMyForm();
    } else {
      this.idEmployee = this.navParams.get("data");
      this.modeText = "Editando: ( " + this.idEmployee + " )";
      //edit
      this.fillFormEdit();
    }

    this.initFormGoup();
  }

  initFormGoup() {
    this.slideOneForm = this.formBuilder.group({
      nickName: [
        "",
        Validators.compose([Validators.maxLength(50), Validators.required])
      ],
      name: [
        "",
        Validators.compose([Validators.maxLength(50), Validators.required])
      ],
      lastName: [
        "",
        Validators.compose([Validators.maxLength(50), Validators.required])
      ],
      document: ["", Validators.compose([Validators.maxLength(50)])],
      contacts: ["", Validators.compose([Validators.maxLength(50)])],
      address: ["", Validators.compose([Validators.maxLength(50)])],
      note: ["", Validators.compose([Validators.maxLength(500)])],
      email: ["", Validators.compose([Validators.maxLength(50)])],
      status: [false],
      nationality: ["", Validators.required]
    });

    this.slideTwoForm = this.formBuilder.group({
      idModeCheckAssistance: ["", Validators.required],
      ignoreWorkContract: [false],
      idSegurityPolicy: ["", Validators.required]
    });
  }
  //#region Manage Evento Forms
  private createNewMyForm() {
    this.slideOneForm = this.formBuilder.group({
      nickName: [
        "",
        Validators.compose([Validators.maxLength(50), Validators.required])
      ],
      name: [
        "",
        Validators.compose([Validators.maxLength(50), Validators.required])
      ],
      lastName: [
        "",
        Validators.compose([Validators.maxLength(50), Validators.required])
      ],
      document: ["", Validators.compose([Validators.maxLength(50)])],
      contacts: ["", Validators.compose([Validators.maxLength(50)])],
      address: ["", Validators.compose([Validators.maxLength(50)])],
      note: ["", Validators.compose([Validators.maxLength(500)])],
      email: ["", Validators.compose([Validators.maxLength(50)])],
      status: [false],
      nationality: ["", Validators.required]
    });

    this.slideTwoForm = this.formBuilder.group({
      idModeCheckAssistance: ["", Validators.required],
      ignoreWorkContract: [false],
      idSegurityPolicy: ["", Validators.required]
    });
  }

  fillFormEdit() {
    this.storage.getEmployeeToId(this.idEmployee).subscribe(data => {
      this.editEmployee = data;
     // console.log(this.editEmployee);

      let status = this.editEmployee.Status == 1 ? true : false;

      this.slideOneForm = this.formBuilder.group({
        nickName: [
          this.editEmployee.NickName,
          Validators.compose([Validators.maxLength(50), Validators.required])
        ],
        name: [
          this.editEmployee.Name,
          Validators.compose([Validators.maxLength(50), Validators.required])
        ],
        lastName: [
          this.editEmployee.LastName,
          Validators.compose([Validators.maxLength(50), Validators.required])
        ],
        document: [
          this.editEmployee.DocumentEmployee,
          Validators.compose([Validators.maxLength(50)])
        ],
        contacts: [
          this.editEmployee.Contacts,
          Validators.compose([Validators.maxLength(50)])
        ],
        address: [
          this.editEmployee.Address,
          Validators.compose([Validators.maxLength(50)])
        ],
        note: [
          this.editEmployee.Note,
          Validators.compose([Validators.maxLength(500)])
        ],
        email: [
          this.editEmployee.Email,
          Validators.compose([Validators.maxLength(50)])
        ],
        status: [status],
        nationality: [
          String(this.editEmployee.IdNationality),
          Validators.required
        ]
      });

      this.slideTwoForm = this.formBuilder.group({
        idModeCheckAssistance: [
          String(this.editEmployee.IdModeCheckAssist),
          Validators.required
        ],
        ignoreWorkContract: [this.editEmployee.IgnoreWorkContract],
        idSegurityPolicy: [
          String(this.editEmployee.IdSegurityPolicy),
          Validators.required
        ]
      });

      this.idCurrency = String(this.editEmployee.IdCurrency);
      this.idGroup = String(this.editEmployee.IdGroup);
      this.idOcupation = String(this.editEmployee.IdOccupation);
      this.idPayRoll = String(this.editEmployee.IdRoster);
    });
  }

  next() {
    this.signupSlider.slideNext();
  }

  prev() {
    this.signupSlider.slidePrev();
  }

  save() {
    this.submitAttempt = true;

    if (!this.slideOneForm.valid) {
      this.signupSlider.slideTo(0);
      this.loading.toastShow("Verifique los Campos!!...");
    } else if (!this.slideTwoForm.valid) {
      this.signupSlider.slideTo(1);
      this.loading.toastShow("Verifique los Campos!!...");
    } else {
      this.sendChangedServer();

      // console.log("success!");
      // console.log(this.slideOneForm.value);
      // console.log(this.slideTwoForm.value);
    }
  }

  sendChangedServer() {
    let profileEmployee: IEmployee;

    if (this.idEmployee == 0) {
      //new Employee
      profileEmployee = {
        Name: this.slideOneForm.value["name"],
        LastName: this.slideOneForm.value["lastName"],
        NickName: this.slideOneForm.value["nickName"],
        DocumentEmployee: this.slideOneForm.value["document"],
        Status: this.slideOneForm.value["status"] ? 1 : 0,
        Contacts: this.slideOneForm.value["contacts"],
        Address: this.slideOneForm.value["address"],
        Note: this.slideOneForm.value["note"],
        Email: this.slideOneForm.value["email"],
        IdNationality: this.slideOneForm.value["nationality"],

        IdModeCheckAssist: this.slideTwoForm.value["idModeCheckAssistance"],
        IgnoreWorkContract: this.slideTwoForm.value["ignoreWorkContract"],
        IdSegurityPolicy: this.slideTwoForm.value["idSegurityPolicy"],

        IdCurrency: Number(this.idCurrency),
        IdGroup: Number(this.idGroup),
        IdRoster: Number(this.idPayRoll),
        IdOccupation: Number(this.idOcupation),

        Base64: "",
        BirthDate: "2019-01-01",
        DateInsert: "2019-01-01",
        ExistFingerPrint: false,
        IdCompany: "",
        IdWorkDay: 88888,
        Idkey: 0,
        PathImg: "user.png",
        Id: this.idEmployee
      };
    } else {
      //EDit employee
      this.editEmployee;
      profileEmployee = {
        Name: this.slideOneForm.value["name"],
        LastName: this.slideOneForm.value["lastName"],
        NickName: this.slideOneForm.value["nickName"],
        DocumentEmployee: this.slideOneForm.value["document"],
        Status: this.slideOneForm.value["status"] ? 1 : 0,
        Contacts: this.slideOneForm.value["contacts"],
        Address: this.slideOneForm.value["address"],
        Note: this.slideOneForm.value["note"],
        Email: this.slideOneForm.value["email"],
        IdNationality: this.slideOneForm.value["nationality"],

        IdModeCheckAssist: this.slideTwoForm.value["idModeCheckAssistance"],
        IgnoreWorkContract: this.slideTwoForm.value["ignoreWorkContract"],
        IdSegurityPolicy: this.slideTwoForm.value["idSegurityPolicy"],

        IdCurrency: Number(this.idCurrency),
        IdGroup: Number(this.idGroup),
        IdRoster: Number(this.idPayRoll),
        IdOccupation: Number(this.idOcupation),

        Base64: "",
        BirthDate: "2019-01-01",
        DateInsert: "2019-01-01",
        ExistFingerPrint: false,
        IdCompany: this.editEmployee.IdCompany,
        IdWorkDay: this.editEmployee.IdWorkDay,
        Idkey: this.editEmployee.Idkey,
        PathImg: this.editEmployee.PathImg,
        Id: this.idEmployee
      };
    }

    if (this.idEmployee == 0) {
      this.sendNewEmployee(profileEmployee);
    } else {
      this.sendEditEmployee(profileEmployee);
    }
  }

  sendNewEmployee(profileEmployee: IEmployee) {
    this.loading.show();
    this.http.checkStatusServer().subscribe(
      data => {
        this.http.SendAddOfUpdateEmployee(profileEmployee).subscribe(data => {
          let result: IResultModelBasic = data;
          if (result.result) {
            profileEmployee.Id = Number(result.msg);
            this.storage.addNewEmployee(profileEmployee).then(data => {
              setTimeout(() => {
                this.loading.hide();
                this.hasChange = true;              
                this.dismissModal();
              }, 3000);
            });
          } else {
            setTimeout(() => {
              this.loading.hide();

              this.loading.toastShow(result.msg);
              // this.dismissModal();
            }, 3000);
          }
        });
      },
      error => {
        setTimeout(() => {
          this.loading.hide();
          this.loading.toastShow("No Tiene Coneccion..");
        }, 3000);
      }
    );
  }

  sendEditEmployee(profileEmployee: IEmployee) {
    this.loading.show();
    this.http.checkStatusServer().subscribe(
      data => {
        this.http.SendAddOfUpdateEmployee(profileEmployee).subscribe(data => {
          let result: IResultModelBasic = data;         
          if (result.result) {
            //profileEmployee.Id = Number(result.msg);
            this.storage.updateEmployeeId(profileEmployee).then(data => {
              setTimeout(() => {
                this.loading.hide();
                this.hasChange = true;
                this.dismissModal();
              }, 1000);
            });
          } else {
            setTimeout(() => {
              this.loading.hide();
              this.loading.toastShow(result.msg);
              //this.dismissModal();
            }, 3000);
          }
        });
      },
      error => {
        setTimeout(() => {
          this.loading.hide();
          this.loading.toastShow("No Tiene Coneccion..");
        }, 3000);
      }
    );
    //console.log(profileEmployee);
  }

  //#endregion

  dismissModal() {
    this.modalCtrl.dismiss({
      dismissed: this.hasChange
    });
  }

  //#region ManagedSelectOptions
  groupChange(event) {
    this.idGroup = event;
  }
  getIdGroupDefault(event) {
    this.idGroup = event;
  }

  currencyChange(event) {
    this.idCurrency = event;
    //console.log(this.idCurrency);
  }
  getIdCurrencyDefault(event) {
    this.idCurrency = event;
    // console.log(this.idCurrency);
  }

  ocupationChange(event) {
    this.idOcupation = event;
    //console.log(this.idOcupation);
  }
  getIdOcupationDefault(event) {
    this.idOcupation = event;
    //console.log(this.idOcupation);
  }

  payRollChange(event) {
    this.idPayRoll = event;
    // console.log(this.idPayRoll);
  }
  getIdPayRollDefault(event) {
    this.idPayRoll = event;
    // console.log(this.idPayRoll);
  }

  //#endregion
}

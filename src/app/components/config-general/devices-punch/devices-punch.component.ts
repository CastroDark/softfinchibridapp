import { Component, OnInit } from "@angular/core";
import { ApiHttpRequestService } from "../../../services/api-http-request.service";
import { IDevicePunch } from "../../../interfaces/public";
import { LoadingService } from "../../../services/loading.service";
import { ActionSheetController, AlertController } from "@ionic/angular";

@Component({
  selector: "app-devices-punch",
  templateUrl: "./devices-punch.component.html",
  styleUrls: ["./devices-punch.component.scss"]
})
export class DevicesPunchComponent implements OnInit {
  public defaultItem: number = 2;
  listDevices: IDevicePunch[] = [];
  modeDevice: string;
  listButtons: any = [];
  nameDevice: string;
  selectDevice: IDevicePunch;
  selectMode: number;
  constructor(
    private http: ApiHttpRequestService,
    private loading: LoadingService,
    private actionCtrl: ActionSheetController,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.defaultItem = 1;
    this.loading.show();
    this.getListDevices();

    this.listButtons = [
      {
        text: "Asistencia",
        role: "destructive",
        icon: "alarm",
        handler: () => {
          this.selectMode = 1;
          this.presentAlertConfirm();
        }
      },
      {
        text: "Pago",
        icon: "cash",
        handler: () => {
          this.selectMode = 2;
          this.presentAlertConfirm();
        }
      },
      {
        text: "Perfil",
        icon: "contacts",
        handler: () => {
          this.selectMode = 3;
          this.presentAlertConfirm();
        }
      },
      {
        text: "Ingreso",
        icon: "calendar",
        handler: () => {
          this.selectMode = 4;
          this.presentAlertConfirm();
        }
      },      
      {
        text: "Cancel",
        icon: "close",
        role: "cancel",
        handler: () => {
          console.log("Cancel clicked");
        }
      }
    ];
  }

  getListDevices() {
    this.http.GetListDevicesPunch("2019-01-01").subscribe(
      data => {
        this.listDevices = data.data;
       // console.log(data);
        setTimeout(() => {
          this.loading.hide();
        }, 200);
      },
      error => {
        setTimeout(() => {
          this.loading.hide();
        }, 200);
        this.loading.toastShow("Error Coneccion..");
      }
    );
  }

  getModeDevice(value: number) {
    switch (value) {
      case 1:
        return "Asistencia";
        break;
      case 2:
        return "Pago";
        break;
      case 3:
        return "Perfil";
        break;
      case 4:
        return "Ingreso";
        break;

      default:
        break;
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionCtrl.create({
      header: this.nameDevice,
      buttons: this.listButtons
    });
    await actionSheet.present();
  }

  showMenu(item) {
    this.selectDevice = item;
    let device: IDevicePunch = item;
    this.nameDevice = item.Name;
    this.presentActionSheet();

    //console.log(item);
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: "Cambiar Modo Lector.?",
      message: " <strong>Estas Seguro </strong>!!!",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {
            //not send data
          }
        },
        {
          text: "Aceptar",
          handler: () => {
            // console.log(this.selectMode);
            //console.log(this.selectDevice);
            this.changeModeDevice();
          }
        }
      ]
    });

    await alert.present();
  }

  changeModeDevice() {
    this.loading.show();
    this.http
      .SendModeDeviceToUpdate(this.selectDevice.Id, this.selectMode)
      .subscribe(
        data => {
          setTimeout(() => {
          this.getListDevices();
          }, 1000);
        },
        error => {
          setTimeout(() => {
            this.loading.toastShow("Error en  Servidor");
          }, 1000);
        }
      );
  }

  restartDevice(item) {

    this.selectDevice = item;
    let device: IDevicePunch = item;
    this.nameDevice = item.Name;
    
    this.loading.show();
    this.http
      .SendModeDeviceToUpdate(this.selectDevice.Id,"restart")
      .subscribe(
        data => {
          setTimeout(() => {
          this.getListDevices();
          }, 1000);
        },
        error => {
          setTimeout(() => {
            this.loading.toastShow("Error en  Servidor");
          }, 1000);
        }
      );
  }



}

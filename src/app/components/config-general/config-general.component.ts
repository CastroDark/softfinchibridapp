import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DevicesPunchComponent } from './devices-punch/devices-punch.component';

@Component({
  selector: 'app-config-general',
  templateUrl: './config-general.component.html',
  styleUrls: ['./config-general.component.scss'],
})
export class ConfigGeneralComponent implements OnInit {


 
  navigate:navigate[]=[{
    icon:"finger-print",
    name:"Ponchadores",
    urlNavigate:"",
    open:1
  }];

  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {}

 
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }


  openModal(value:number){
       switch (value) {
         case 1:
          this.showModalDevices();
           break;
       
         default:
           break;
       }

  }


  //#managed Modal Devices Punch
 async showModalDevices(){

  const modal = await this.modalCtrl.create({
     component:DevicesPunchComponent,
     cssClass:"wideModal"
  });
  return await modal.present();
  }


  //#endregion 
}
interface navigate{
  urlNavigate:string,
  icon:string,
  name:string,
  open:number
}

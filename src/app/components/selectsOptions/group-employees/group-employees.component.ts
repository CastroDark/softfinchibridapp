import {
  Component,
  OnInit,
  SimpleChanges,
  Output,
  Input,
  EventEmitter
} from "@angular/core";
import { IGroupEmployee } from "src/app/interfaces/public";
import { Storage } from "@ionic/storage";
import { DbStorageService } from "../../../services/db-storage.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-group-employees",
  templateUrl: "./group-employees.component.html",
  styleUrls: ["./group-employees.component.scss"]
})
export class GroupEmployeesComponent implements OnInit {
  @Input() showAllOpcion: number;
  @Input() getIdDefault: string;
  @Output() emitChangeGroup = new EventEmitter();
  @Output() emitSendIdDefault = new EventEmitter();

  idDefault: string;
  showAll: boolean;

  listGroups: Array<IGroupEmployee>;

  constructor(private storage: DbStorageService) {}

  ngOnInit() {
    
    this.getList().subscribe(data => {      
      //select if show all Options
      if (this.showAllOpcion == 1) {
        this.showAll = true;
        this.idDefault = "0";
      } else { 
        if(this.getIdDefault){
          this.idDefault = this.getIdDefault
        }  else{
          this.idDefault = String(this.listGroups[0].Id);
        }    
       
      }
      this.sendIdDefault(this.idDefault);
    });
  }

  getList(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.storage.getListAllGroupsEmployees().subscribe(data => {
        let dblist: Array<IGroupEmployee> = data;
        this.listGroups = dblist;
        observe.next(true);
      });
    });

    return result;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["showAllOpcion"]) {
      setTimeout(() => {
        //   console.log(this.showAllOpcion);
        //   this.showAll =true; //this.showAllOpcion;
        //   //console.log(this.showAll);
      }, 2000);
    }
  }

  sendIdDefault(idDefault) {
    this.emitSendIdDefault.emit(idDefault);
  }

  changeValue(event) {
    this.emitChangeGroup.emit(this.idDefault);
  }
}

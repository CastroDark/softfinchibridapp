import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DbStorageService } from 'src/app/services/db-storage.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
})
export class StatusComponent implements OnInit {
  @Input() showAllOpcion: number;
  @Input() getIdDefault: string;
  @Output() emitChangeGroup = new EventEmitter();
  @Output() emitSendIdDefault = new EventEmitter();

  idDefault: string;
  showAll: boolean;

  listOptions=[{
    Id:"1",
    Name:"  A C T I V O  "
  },
  {
    Id:"2",
    Name:"  I N A C T I V O  "
  }];

  constructor(private storage: DbStorageService) {}

  
  ngOnInit() {
    this.getList().subscribe(data => {      
      //select if show all Options
      if (this.showAllOpcion == 1) {
        this.showAll = true;
        this.idDefault = "0";
      } else { 
        if(this.getIdDefault){
          this.idDefault = this.getIdDefault
        }  else{
          this.idDefault = String(this.listOptions[0].Id);
        }    
       
      }
      this.sendIdDefault(this.idDefault);
    });
  }

  getList(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {  
      observe.next(true);
    });

    return result;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["showAllOpcion"]) {
      setTimeout(() => {
        //   console.log(this.showAllOpcion);
        //   this.showAll =true; //this.showAllOpcion;
        //   //console.log(this.showAll);
      }, 2000);
    }
  }

  sendIdDefault(idDefault) {
    this.emitSendIdDefault.emit(idDefault);
  }

  changeValue(event) {
    this.emitChangeGroup.emit(this.idDefault);
  }

}

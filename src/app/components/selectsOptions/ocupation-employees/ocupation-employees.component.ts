import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { IOccupationEmployee } from '../../../interfaces/public';
import { DbStorageService } from 'src/app/services/db-storage.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ocupation-employees',
  templateUrl: './ocupation-employees.component.html',
  styleUrls: ['./ocupation-employees.component.scss'],
})
export class OcupationEmployeesComponent implements OnInit {
  @Input() showAllOpcion: number;
  @Input() getIdDefault: string;
  @Output() emitChangeGroup = new EventEmitter();
  @Output() emitSendIdDefault = new EventEmitter();
  idDefault: string;
  showAll: boolean;

  listGroups: Array<IOccupationEmployee>;
  constructor(private storage: DbStorageService) { }

  ngOnInit() {
      
    this.getList().subscribe(data => {      
      //select if show all Options
      if (this.showAllOpcion == 1) {
        this.showAll = true;
        this.idDefault = "0";
      } else { 
        if(this.getIdDefault){
          this.idDefault = this.getIdDefault
        }  else{
          this.idDefault = String(this.listGroups[0].Id);
        }    
       
      }
      this.sendIdDefault(this.idDefault);
    });
  }

  getList(): Observable<boolean> {
    const result = new Observable<boolean>(observe => {
      this.storage.getListAllOccupationEmployees().subscribe(data => {
        let dblist: Array<IOccupationEmployee> = data;
        this.listGroups = dblist;
        observe.next(true);
      });
    });

    return result;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["showAllOpcion"]) {
      setTimeout(() => {
        //   console.log(this.showAllOpcion);
        //   this.showAll =true; //this.showAllOpcion;
        //   //console.log(this.showAll);
      }, 2000);
    }
  }

  sendIdDefault(idDefault) {
    this.emitSendIdDefault.emit(idDefault);
  }

  changeValue(event) {
    this.emitChangeGroup.emit(this.idDefault);
  }

}

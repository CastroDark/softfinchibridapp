import { NgModule, Input, Output, EventEmitter } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HideHeaderDirective } from "../directives/hide-header.directive";
import { DatetimeComponent } from "./datetime/datetime.component";
import { IonicModule } from "@ionic/angular";

import { GroupEmployeesComponent } from "./selectsOptions/group-employees/group-employees.component";
import { CapitalizePipe } from "../pipes/capitalize.pipe";
import { DetailsPunchEmployeeComponent } from './modals/details-punch-employee/details-punch-employee.component';
import { DatetimePipe } from '../pipes/datetime.pipe';
import { ProfileEmployeeComponent } from './modals/profile-employee/profile-employee.component';
import { ConfigGeneralComponent } from './config-general/config-general.component';
import { DevicesPunchComponent } from './config-general/devices-punch/devices-punch.component';
import { SelectModeDeviceComponent } from './select-mode-device/select-mode-device.component';
import { FormsEmployeesComponent } from './modals/forms-employees/forms-employees.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrencysComponent } from './selectsOptions/currencys/currencys.component';
import { OcupationEmployeesComponent } from './selectsOptions/ocupation-employees/ocupation-employees.component';
import { PaysRollComponent } from './selectsOptions/pays-roll/pays-roll.component';
import { StatusComponent } from './selectsOptions/status/status.component';
@NgModule({
  declarations: [
    HideHeaderDirective,
    DatetimeComponent,
    GroupEmployeesComponent,
    CapitalizePipe,DetailsPunchEmployeeComponent,
    DatetimePipe,
    ProfileEmployeeComponent,
    ConfigGeneralComponent,
    DevicesPunchComponent,
    SelectModeDeviceComponent,
    FormsEmployeesComponent,
    CurrencysComponent,
    OcupationEmployeesComponent,
    PaysRollComponent,
    StatusComponent
    
   
  
  ],
  entryComponents:[DetailsPunchEmployeeComponent,
     ProfileEmployeeComponent,
     ConfigGeneralComponent,
     DevicesPunchComponent,
     FormsEmployeesComponent
    
  ],
  imports: [CommonModule, IonicModule, FormsModule,FormsModule, ReactiveFormsModule],
  exports: [
    HideHeaderDirective,
    DatetimeComponent,
    GroupEmployeesComponent,
    CapitalizePipe,
    SelectModeDeviceComponent,
    CurrencysComponent,
    OcupationEmployeesComponent,
    PaysRollComponent,
    StatusComponent
    
  
   
  ],
  providers:[]
})
export class ComponentModule {}

import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { IonicStorageModule } from "@ionic/storage";
import { ComponentModule } from "./components/component.module";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { HttpInterceptorService } from "./services/http-interceptor.service";
import { UniqueDeviceID } from "@ionic-native/unique-device-id/ngx";
import { File } from "@ionic-native/file/ngx";
import { Base64 } from "@ionic-native/base64/ngx";
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    ComponentModule,
    HttpClientModule,
    FormsModule,
     ReactiveFormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UniqueDeviceID,
    File,
    Base64,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    FileOpener,
    FileTransfer,
    FileTransferObject 

   
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

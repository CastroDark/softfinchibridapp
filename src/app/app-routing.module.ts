import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'splash', loadChildren: './pages/splash/splash.module#SplashPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'block-app', loadChildren: './pages/block-app/block-app.module#BlockAppPageModule' },
  { path: 'authorize-phone', loadChildren: './pages/authorize-phone/authorize-phone.module#AuthorizePhonePageModule' },
  { path: 'assistance-employees', loadChildren: './pages/employees/assistance-employees/assistance-employees.module#AssistanceEmployeesPageModule' },
  { path: 'push-online', loadChildren: './pages/employees/push-online/push-online.module#PushOnlinePageModule' },
  { path: 'update-app', loadChildren: './pages/update-app/update-app.module#UpdateAppPageModule' },
  { path: 'manage-employees', loadChildren: './pages/employees/manage-employees/manage-employees.module#ManageEmployeesPageModule' },
 ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

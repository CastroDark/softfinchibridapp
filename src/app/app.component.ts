import { Component } from "@angular/core";

import { Platform, ModalController } from '@ionic/angular';
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { UniqueDeviceID } from "@ionic-native/unique-device-id/ngx";
import { Router } from "@angular/router";
import { ConfigGeneralComponent } from './components/config-general/config-general.component';

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {
  public pages = [
    {
      title: "Inicio",
      url: "/home",
      icon: "home",
      open: false
    },
    {
      title: "Empleados",
      children: [
        {
          title: "Empleados",
          url: "/manage-employees",
          icon: "contacts",
          open: false
        },
        {
          title: "Asistencia",
          url: "/assistance-employees",
          icon: "alarm",
          open: false
        },
        {
          title: "Ponche",
          url: "/push-online",
          icon: "finger-print",
          open: false
        }
        
      ]
    },
    {
      title: "Dispositivos Ponche",
      children: [
        {
          title: "Asistencia",
          url: "/assistance-employees",
          icon: "logo-ionic",
          open: false
        },
        {
          title: "PoncheOnline",
          url: "/push-online",
          icon: "finger-print",
          open: false
        }
        
      ]
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private modalCtrl:ModalController
   
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page,open) {
    //console.log(open)
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    setTimeout(() => {
      this.router.navigate([page]);
    }, 200);
  }

  //#region Modal config

  showModalConfigGeneral(){
    this.presentModal()
  }
  async presentModal() {
    const modal = await this.modalCtrl.create({
      component:ConfigGeneralComponent,  
    });
    return await modal.present();
  }



  //#endregion
}

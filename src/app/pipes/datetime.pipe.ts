import { Pipe, PipeTransform } from "@angular/core";
import { DatetimeComponent } from "../components/datetime/datetime.component";
import { DatetimeService } from "../services/datetime.service";
import { Observable } from "rxjs";

@Pipe({
  name: "datetime"
})
export class DatetimePipe implements PipeTransform {
  dateTime: string = "";
  constructor(private dateCtrl: DatetimeService) {}

  transform(value: any): Observable<any> {
    const send = new Observable<any>(observe => {
      this.dateCtrl.getFormatDate(value).subscribe(data => {
        this.dateTime = data;

        this.dateCtrl.getFormatDateTime(value).subscribe(data => {
          //console.log(this.dateTime);
          this.dateTime = this.dateTime + "  ...   " + data;

          observe.next(this.dateTime);
        });
      });

      // this.dateTime.getFormatDate(value).subscribe(async data=>{
      //              dateTime=data;
      //      this.dateTime.getFormatDateTime(value).subscribe(data=>{
      //       this.dateTime=dateTime=dateTime +'  ' + data;
      //     observe.next(this.dateTime);

      //      });
      //  });
    });

    return send;
  }
}

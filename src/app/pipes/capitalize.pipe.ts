import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(word: any): any {
    
    if (!word) return word;

    // var sd =this.ucFirstAllWords(word )
    // console.log(sd)

    return this.ucFirstAllWords(word);

    return word.charAt(0).toUpperCase() + word.slice(1);

    //return  word.charAt(0).toUpperCase() + word.slice(1)
  //return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

   ucFirstAllWords( str )
{
    var pieces = str.split(" ");
    for ( var i = 0; i < pieces.length; i++ )
    {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1).toLowerCase();
    }
    return pieces.join(" ");
}


}
